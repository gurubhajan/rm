"use strict";

const BTN_COLOR = "#ffffff";
const BTN_BACKGROUND_COLOR = "#0d5eff";
const BTN_WIDTH = 225;
const BTN_HEIGHT = 80;
const BTN_BORDER_RADIUS = "0.75em";
const BTN_SIZE = 80;
const ANIMATION_TIME = 0.4;
const UPLOADING_BACKGROUND_COLOR = "#ffffff";
const UPLOADING_WIDTH = 320;
const UPLOADING_HEIGHT = 30;
const DEFAULT_CONTAINER_BACKGROUND_COLOR = "#143982";
const DEFAULT_BTN_UPLOAD_BACKGROUND_COLOR = "#0d5eff";
const COMPLETE_CONTAINER_BACKGROUND_COLOR = "transparent";
const COMPLETE_BTN_UPLOAD_BACKGROUND_COLOR = "#0fe600";
const COMPLETE_SIZE = 80;
const CHECK_WIDTH = "100%";
const CHECK_HEIGHT = "100%";
const PROGRESS_TIME = 2;
const container = document.querySelector("#container");
const btnUpload = document.querySelector("#btn_upload");
const progress = btnUpload.querySelector(".progress");
const check = btnUpload.querySelector(".check");
const check1 = btnUpload.querySelector(".check > span:first-child");
const check2 = btnUpload.querySelector(".check > span:last-child");
const tl = gsap.timeline();

let uploading = false;

function uploadStart() {
    // The button will become circle
    // The UPLOAD text will fade
    tl.to(btnUpload, {
        width: BTN_SIZE,
        height: BTN_SIZE,
        color: "transparent",
        fill: "transparent",
        borderRadius: "50%",
        duration: ANIMATION_TIME,
        ease: "elastic.out(1, 0.3)"
    });

    // The button will become a progress bar
    tl.to(btnUpload, {
        backgroundColor: UPLOADING_BACKGROUND_COLOR,
        width: UPLOADING_WIDTH,
        height: UPLOADING_HEIGHT,
        borderRadius: "0.25em",
        duration: ANIMATION_TIME,
        ease: "power2.out",
    });

    // Render the progress "speech bubble"
    tl.to(progress, {
        display: "flex"
    });

    // Show the progress "speech bubble" (Fade in)
    tl.to(progress, {
        opacity: 1,
        duration: ANIMATION_TIME
    });
}

function uploadProcess() {
    let uploadProgress = {
        val: 0
    };

    // Change the progress of upload
    tl.to(uploadProgress, PROGRESS_TIME, {
        val: 100,
        ease: "power4.inOut",
        onStart: function() {
            // Tilt the progress "speech bubble"
            tl.to(progress, {
                transform: "translateX(-50%) rotate(12deg)",
                delay: 0,
                ease: "power2.out"
            }, "-=" + PROGRESS_TIME);
        },
        onUpdate: function() {
            let value = Math.floor(uploadProgress.val);

            // Increase the value of the progress (blue colored progress on progress bar)
            btnUpload.style.backgroundImage = "linear-gradient(to right, " +
                BTN_BACKGROUND_COLOR + " 0 " + value + "%, transparent " + value + "% 0)";

            // Change the text of the progress "speech bubble"
            progress.innerText = value + "%";

            // Change the position of the progress "speech bubble"
            progress.style.left = (UPLOADING_WIDTH * (value / 100)) + "px";
        },
        onComplete: function() {
            // Remove the tilting of the progress "speech bubble"
            tl.to(progress, {
                transform: "translateX(-50%)",
            });

            uploadEnd();
        }
    });
}

function uploadEnd() {
    // Hide the progress "speech bubble" (Fade out)
    tl.to(progress, {
        opacity: 0,
        duration: ANIMATION_TIME,
        ease: "power2.out",
        onComplete: function() {
            btnUpload.style.backgroundImage = null;
            btnUpload.style.backgroundColor = BTN_BACKGROUND_COLOR;
        }
    });

    // Change the background color of the container
    tl.to(container, {
        backgroundColor: COMPLETE_CONTAINER_BACKGROUND_COLOR,
        duration: ANIMATION_TIME,
        ease: "power2.out",
    });

    // Change the background color, as well as the shape of the upload button
    // As the same time as the background color of the contaianer changes
    tl.to(btnUpload, {

        width: COMPLETE_SIZE,
        height: COMPLETE_SIZE,
        borderRadius: "50%",
        duration: ANIMATION_TIME,
        ease: "back.out",
        onComplete: function() {
            check.style.display = "block";
        }
    }, "-=" + ANIMATION_TIME);

    // Show the tail of the completed check mark
    tl.to(check1, {
        height: CHECK_HEIGHT,
        duration: ANIMATION_TIME,
        ease: "power2.out"
    });

    // Show the body of the completed check mark
    tl.to(check2, {
        width: CHECK_WIDTH,
        duration: ANIMATION_TIME,
        ease: "bounce.out",
        onComplete: function() {
            setTimeout(function() {
                // After 5000 milliseconds, return the button and
                // the container into original state
                initialize();
            }, 5000);
        }
    });
}

function initialize() {
    uploading = false;

    btnUpload.style.backgroundImage = null;

    progress.style.left = "0px";
    progress.innerText = "";

    tl.to(container, {

        duration: ANIMATION_TIME,
        ease: "power2.out",
    });

    tl.to(btnUpload, {
        color: BTN_COLOR,
        fill: BTN_COLOR,

        width: BTN_WIDTH,
        height: BTN_HEIGHT,
        borderRadius: BTN_BORDER_RADIUS,
        duration: ANIMATION_TIME,
        ease: "bounce.out"
    }, "-=" + ANIMATION_TIME);

    tl.to(check, {
        opacity: 0,
        duration: ANIMATION_TIME,
        ease: "bounce.out",
        onComplete: function() {
            check.style.opacity = 1;
            check.style.display = "none";

            check1.style.height = "0px";

            check2.style.width = "0px";
        }
    }, "-=" + ANIMATION_TIME);
}

btnUpload.addEventListener("click", function() {
    if (uploading === true) {
        return;
    }

    uploading = true;

    this.classList.add("btn-upload-uploading");

    uploadStart();
    uploadProcess();
});
//////****************************************************















const $ = (s, o = document) => o.querySelector(s);
const $$ = (s, o = document) => o.querySelectorAll(s);

$$('.button').forEach(button => {

    let count = {
            number: 0
        },
        icon = $('.icon', button),
        iconDiv = $('.icon > div', button),
        arrow = $('.icon .arrow', button),
        countElem = $('span', button),
        svgPath = new Proxy({
            y: null,
            s: null,
            f: null,
            l: null
        }, {
            set(target, key, value) {
                target[key] = value;
                if (target.y !== null && target.s != null && target.f != null && target.l != null) {
                    arrow.innerHTML = getPath(target.y, target.f, target.l, target.s, null);
                }
                return true;
            },
            get(target, key) {
                return target[key];
            }
        });

    svgPath.y = 30;
    svgPath.s = 0;
    svgPath.f = 8;
    svgPath.l = 32;

    button.addEventListener('click', e => {
        if (!button.classList.contains('loading')) {

            if (!button.classList.contains('animation')) {

                button.classList.add('loading', 'animation');

                gsap.to(svgPath, {
                    f: 2,
                    l: 38,
                    duration: .3,
                    delay: .15
                });

                gsap.to(svgPath, {
                    s: .2,
                    y: 16,
                    duration: .8,
                    delay: .15,
                    ease: Elastic.easeOut.config(1, .4)
                });

                gsap.to(count, {
                    number: '100',
                    duration: 3.8,
                    delay: .8,
                    onUpdate() {
                        countElem.innerHTML = Math.round(count.number) + '%';
                    }
                });

                setTimeout(() => {
                    iconDiv.style.setProperty('overflow', 'visible');
                    setTimeout(() => {
                        button.classList.remove('animation');
                    }, 600);
                }, 4820);

            }

        } else {

            if (!button.classList.contains('animation')) {

                button.classList.add('reset');

                gsap.to(svgPath, {
                    f: 8,
                    l: 32,
                    duration: .4
                });

                gsap.to(svgPath, {
                    s: 0,
                    y: 30,
                    duration: .4
                });

                setTimeout(() => {
                    button.classList.remove('loading', 'reset');
                    iconDiv.removeAttribute('style');
                }, 400);

            }

        }
        e.preventDefault();
    });

});

function getPoint(point, i, a, smoothing) {
    let cp = (current, previous, next, reverse) => {
            let p = previous || current,
                n = next || current,
                o = {
                    length: Math.sqrt(Math.pow(n[0] - p[0], 2) + Math.pow(n[1] - p[1], 2)),
                    angle: Math.atan2(n[1] - p[1], n[0] - p[0])
                },
                angle = o.angle + (reverse ? Math.PI : 0),
                length = o.length * smoothing;
            return [current[0] + Math.cos(angle) * length, current[1] + Math.sin(angle) * length];
        },
        cps = cp(a[i - 1], a[i - 2], point, false),
        cpe = cp(point, a[i - 1], a[i + 1], true);
    return `C ${cps[0]},${cps[1]} ${cpe[0]},${cpe[1]} ${point[0]},${point[1]}`;
}

function getPath(update, first, last, smoothing, pointsNew) {
    let points = pointsNew ? pointsNew : [
            [first, 16],
            [20, update],
            [last, 16]
        ],
        d = points.reduce((acc, point, i, a) => i === 0 ? `M ${point[0]},${point[1]}` : `${acc} ${getPoint(point, i, a, smoothing)}`, '');
    return `<path d="${d}" />`;
}







/////************************//////////////