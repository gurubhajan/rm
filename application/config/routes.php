<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// User Routings
$route['register'] = 'user/login';
$route['logout'] = 'user/logout';
$route['sign_in'] = 'user/sign_in';
$route['register'] = 'user/register';
$route['reg-user'] = 'user/reg_user';
$route['dashboard'] = 'user/dashboard';
$route['user/user-profile'] = 'user/manage_profile';
$route['user/data-generator'] = 'user/data_generator';
$route['user/new-campaign'] = 'user/campaign';
$route['user/campaign-list'] = 'user/campaign_list';
$route['user/running-campaign'] = 'user/running_campaign';
$route['user/schedule-campaign'] = 'user/schedule_campaign';
$route['user/buy-credit'] = 'user/buy_credit';
$route['user/transaction-history'] = 'user/transactions';
$route['user/create-ticket'] = 'user/create_ticket';
$route['user/ticket-list'] = 'user/ticket_list';
$route['user/reg_ticket'] = 'user/reg_ticket';

// Admin Routings
$route['admin/default-setting'] = 'admin/default_setting';
$route['admin/update_dsetting'] = 'admin/update_dsetting';
$route['admin/users'] = 'admin/users';
$route['admin/packagecredit'] = 'admin/packagecredit';
$route['admin/notificationprofile'] = 'admin/notificationprofile';
$route['admin/tickets'] = 'admin/tickets';