<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct() {
        parent::__construct();
        //$this->load->helper('search');
        $this->load->helper('url');
        $this->load->library('form_validation');
		$this->load->library('pagination');
        $this->load->model("admin_model");
        $this->load->library('session');
        // Load file helper
        $this->load->helper('file');
    }
	
    public function headers()
    {
    	$data = "*";
		$where = Null;
		$res = $this->admin_model->selectRecord(TABLE_PREFIX."_users",$data,$where);
		$result['trek_users'] = $res->row();

		$this->load->view("admin_views/header",$result);
    }

    public function index()
    {	
    	$message = array("class" => "", "msg" => "");
        $id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->load->view('admin_views/login',$message);
        } else {
            $this->dashboard();
        }
    }

    public function login($message)
	{
		if(!isset($message)){
			$message = array("class" => "", "msg" => "");
		}
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->load->view('admin_views/login',$message);
        } else {
            $this->dashboard();
        }
	}

    public function sign_in()
    {
        $rss = $this->admin_model->check_user_status($this->input->post('email'), $this->input->post('password'));
		//echo '<pre>'.print_r($rss).'</pre>';
		if(!empty($rss)){
			if($rss->status==0){
			// 	$message = array("class" => "danger", "msg" => "You are not an Admin User");
	  //           $this->login($message);
			// }else{
				$res = $this->admin_model->login_user($this->input->post('email'), $this->input->post('password'));
				if (!empty($res)) {
					redirect(site_url("admin/dashboard"));
				} else {
					$message = array("class" => "danger", "msg" => "Invalid login credentials");
					$this->login($message);
				}
			}
		} else {
			$message = array("class" => "danger", "msg" => "It seems your email isn't in our records");
	        $this->login($message);
		}
    }

	public function dashboard()
	{	
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			//$result['users_count'] = $this->admin_model->selectRecord(TABLE_PREFIX."_certificates",'count(id) as all_users',NULL)->row()->all_users;
			$result['users_count'] = $this->admin_model->selectRecord(TABLE_PREFIX."_users",'count(id) as all_users',NULL)->row()->all_users;
			$result['get_all_users'] = $this->admin_model->selectRecordbyDesc(TABLE_PREFIX."_users", $data, array('role'=>0))->result();
			$result['dash'] = "selected";
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/dashboard', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function edit_profile($result=NULL)
	{	
		$result = array("class" => "", "msg" => "");
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['editp'] = "selected";
			$result['user_pdata'] = $this->admin_model->selectRecord(TABLE_PREFIX."_user_details", $data, array( 'admin_id' => $id ))->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/edit-profile', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function change_stat()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$user_stat = $this->input->post("user_stat");
        	$myarray = explode(',', $user_stat);

        	$data1 = "*";
			$where1 = array( "id" => $myarray[1] );
			$res = $this->admin_model->selectRecord(TABLE_PREFIX."_users",$data1,$where1);
			$res = $res->row();
			$data = array("status"=>$myarray[0]);
			$where = array( "id" => $myarray[1] );
			$result = $this->admin_model->update(TABLE_PREFIX."_users",$data,$where);
			echo $result;
			//$this->dashboard();
		}
	}

	public function default_setting()
	{
		//die(print_r($result));
		// $result = array("class" => "", "msg" => "");
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['setdefset'] = "selected";
			$result['default_setting'] = $this->admin_model->selectRecord(TABLE_PREFIX."_default_setting", $data, array('id' => '1'))->row();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/default-setting', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function update_dsetting()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = array(
						"default_credit" => $this->input->post("d_credit"),
						"price_per_credit" => $this->input->post("pp_credit"),
					);
        	$where = array( "id" => '1');
			$result1 = $this->admin_model->update(TABLE_PREFIX."_default_setting",$data,$where);
		
			if ($result1){
				if ($result1 == 0) {
                    $result = array("class" => "danger", "msg" => "Failed to update details");
					echo json_encode($result);
                }
                if ($result1 == 1) {
                    $result = array("class" => "success", "msg" => "Details Updated Successfully");
					echo json_encode($result);
                }
			}
		}
	}

	public function users()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			$result['subscribers'] = "selected";
			$result['users'] = $this->admin_model->selectRecord(TABLE_PREFIX."_users", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/subscribers', $result);
			$this->load->view("admin_views/footer");
		}
	}
	
	public function packagecredit()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			$result['set_pack_credit'] = "selected";
			$result['package_credit_list'] = $this->admin_model->selectRecord(TABLE_PREFIX."_package_credit_list", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/packagecredit', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function notificationprofile()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			$result['notification_prof'] = "selected";
			$result['notification_profile_list'] = $this->admin_model->selectRecord(TABLE_PREFIX."_notification_profile", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/notification_profile', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function tickets()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			$result['utickets'] = "selected";
			$result['users'] = $this->admin_model->selectRecord(TABLE_PREFIX."_users", $data, NULL)->result();
			$result['tickets'] = $this->admin_model->selectRecord(TABLE_PREFIX."_ticket", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/tickets', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function all_slides()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['alslid'] = "selected";
			$result['get_all_slides'] = $this->admin_model->selectRecord(TABLE_PREFIX."_slides", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/all_sliders', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function add_slider()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$result['adslid'] = "selected";
			$result['data'] = "add";
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/add_slides', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function save_slides()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			if($_FILES['slider']['name']!=''){
				$target_dir = "uploads/slides/";
				$target_file = $target_dir .time()."_".basename($_FILES['slider']['name']);
				$name= time()."_".basename($_FILES['slider']['name']);
				move_uploaded_file($_FILES["slider"]["tmp_name"], $target_file);
			}else{
				$name = $this->input->post("slide_val");
			}
			
            $data = array(
						"title" =>$this->input->post("slider_title"),
						"text" =>$this->input->post("slider_text"),
						"image" => $name,
						//"slide_text" => str_replace("?","",$this->input->post("slide_text")),
					);
			$res = $this->admin_model->insert(TABLE_PREFIX."_slides",$data);
			if($res){
				redirect(site_url("admin/all_slides"));
			}else{
				redirect(site_url("admin/all_slides"));
			}
        }
	}

	public function edit_slider($cid)
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			// $res['slide'] = "active";
			// $res['add_slide'] = "active";
			// $res['slide_menu'] = "menu-open";
            $data = "*";
			$where = array( "id" => $cid );
			$result['data'] = $this->admin_model->selectRecord(TABLE_PREFIX."_slides",$data,$where);
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/add_slides', $result);
			$this->load->view("admin_views/footer");
        }
	}

	public function update_slides()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			if($_FILES['slider']['name']!=''){
				$target_dir = "uploads/slides/";
				$target_file = $target_dir .time()."_".basename($_FILES['slider']['name']);
				$name=time()."_".basename($_FILES['slider']['name']);
				move_uploaded_file($_FILES["slider"]["tmp_name"], $target_file);
			}else{
				$name = $this->input->post("slide_val");
			}
			
            $data = array(
						"title" =>$this->input->post("slider_title"),
						"text" =>$this->input->post("slider_text"),
						"image" => $name,
						//"slide_text" => str_replace("?","",$this->input->post("slide_text")),
					);
			$where = array( "id" => $this->input->post("id") );
			$res = $this->admin_model->update(TABLE_PREFIX."_slides",$data,$where);
			if($res){
				redirect(site_url("admin/all_slides"));
			}else{
				redirect(site_url("admin/all_slides"));
			}
        }
	}

	public function AllProjects()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['alprjcts'] = "selected";
			$result['all_projects'] = $this->admin_model->selectRecord(TABLE_PREFIX."_projects", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/all_projects', $result);
			$this->load->view("admin_views/footer");
		}
	}
	public function AddProject()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$result['adprjcts'] = "selected";
			$result['data'] = "add";
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/add_projects', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function saveCertificate()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			if($_FILES['proj_img_1']['name']!=''){
				$target_dir = "uploads/proj_img_1/";
				$target_file = $target_dir .time()."_".basename($_FILES['proj_img_1']['name']);
				$proj_img_1= time()."_".basename($_FILES['proj_img_1']['name']);
				move_uploaded_file($_FILES["proj_img_1"]["tmp_name"], $target_file);
			}else{
				$proj_img_1 = $this->input->post("proj_img_1_val");
			}

			if($_FILES['proj_img_2']['name']!=''){
				$target_dir = "uploads/proj_img_2/";
				$target_file = $target_dir .time()."_".basename($_FILES['proj_img_2']['name']);
				$proj_img_2= time()."_".basename($_FILES['proj_img_2']['name']);
				move_uploaded_file($_FILES["proj_img_2"]["tmp_name"], $target_file);
			}else{
				$proj_img_2 = $this->input->post("proj_img_2_val");
			}
			$unique_ID = "ipgl".rand(10000000,9999999999);
            $data = array(
				"description" =>$this->input->post("description"),
				"description" =>$this->input->post("description"),
						"diamond_weight" =>$this->input->post("diamond_weight"),
						"gross_weight" =>$this->input->post("gross_weight"),
						"shape" =>$this->input->post("shape"),
						"finish" =>$this->input->post("finish"),
						"colour" =>$this->input->post("colour"),
						"clarity" =>$this->input->post("clarity"),
						"additional_notes" => $this->input->post("additional_notes"),
						"proj_img_1" => $proj_img_1,
						"proj_img_2" => $proj_img_2,
						//"slide_text" => str_replace("?","",$this->input->post("slide_text")),
					);
			$res = $this->admin_model->insert(TABLE_PREFIX."_certificates",$data);
			if($res){
				redirect(site_url("admin/allCertificates"));
			}else{
				redirect(site_url("admin/allCertificates"));
			}
        }
	}

	public function allCertificates()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['alcert'] = "selected";
			$result['all_certificates'] = $this->admin_model->selectRecord(TABLE_PREFIX."_certificates", $data, NULL)->result();
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/all_certificates', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function updateCertificate()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			if($_FILES['proj_img_1']['name']!=''){
				$target_dir = "uploads/proj_img_1/";
				$target_file = $target_dir .time()."_".basename($_FILES['proj_img_1']['name']);
				$proj_img_1= time()."_".basename($_FILES['proj_img_1']['name']);
				move_uploaded_file($_FILES["proj_img_1"]["tmp_name"], $target_file);
			}else{
				$proj_img_1 = $this->input->post("product_img_val");
			}

			if($_FILES['proj_img_2']['name']!=''){
				$target_dir = "uploads/proj_img_2/";
				$target_file = $target_dir .time()."_".basename($_FILES['proj_img_2']['name']);
				$proj_img_2= time()."_".basename($_FILES['proj_img_2']['name']);
				move_uploaded_file($_FILES["proj_img_2"]["tmp_name"], $target_file);
			}else{
				$proj_img_2 = $this->input->post("proj_img_2_val");
			}
			$unique_ID = "ipgl".rand(10000000,9999999999);
            $data = array(
            			"certificate_id" => $unique_ID,
						"description" =>$this->input->post("description"),
						"diamond_weight" =>$this->input->post("diamond_weight"),
						"gross_weight" =>$this->input->post("gross_weight"),
						"shape" =>$this->input->post("shape"),
						"finish" =>$this->input->post("finish"),
						"colour" =>$this->input->post("colour"),
						"clarity" =>$this->input->post("clarity"),
						"additional_notes" => $this->input->post("additional_notes"),
						"proj_img_1" => $proj_img_1,
						"proj_img_2" => $proj_img_2,
						//"slide_text" => str_replace("?","",$this->input->post("slide_text")),
					);
			$res = $this->admin_model->insert(TABLE_PREFIX."_certificates",$data);
			if($res){
				redirect(site_url("admin/allCertificates"));
			}else{
				redirect(site_url("admin/allCertificates"));
			}
        }
	}

	public function addCertificate()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			$result['adcert'] = "selected";
			$result['data'] = "add";
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/add_certificates', $result);
			$this->load->view("admin_views/footer");
		}
	}

	public function uadmin_info()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = array(
						"phone" => $this->input->post("p_phno"),
						"sec_email" => $this->input->post("p_email"),
						"address" => $this->input->post("p_address"),
						"f_url" => $this->input->post("f_url"),
						"t_url" => $this->input->post("t_url"),
						"l_url" => $this->input->post("l_url"),
					);
        	$where = array( "admin_id" => $this->session->userdata("admin_id") );
        	$result1 = $this->admin_model->update(TABLE_PREFIX."_user_details",$data,$where);
			// $p_phno = $this->input->post("p_phno");
			// $p_email = $this->input->post("p_email");
			// $p_address = $this->input->post("p_address");
			// $f_url = $this->input->post("f_url");
			// $t_url = $this->input->post("t_url");
			// $l_url = $this->input->post("l_url");
			if ($result1){
				if ($result1 == 0) {
                    $result = array("class" => "danger", "msg" => "Failed to update your contact details");
                    $this->edit_profile($result);
                }
                if ($result1 == 1) {
                    $result = array("class" => "success", "msg" => "Details Updated Successfully");
                    $this->edit_profile($result);
                }

			}
		}
	}

	public function newsletter_emails()
	{
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
			$result['get_all_emails'] = $this->admin_model->selectRecord(TABLE_PREFIX."_newsletter", $data, NULL)->result();
			//$result['users_data'] = $this->admin_model->getUserDetails();
			$result['nwsltr'] = "selected";
			$this->headers();
			$this->load->view('admin_views/sidebar', $result);
			$this->load->view('admin_views/news_ltr', $result);
			$this->load->view("admin_views/footer");
		}
	}

	// Export data in CSV format 
	public function exportCSV()
	{ 
		// file name 
		$filename = 'subcribed_users_'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

		// get data 
		$usersData = $this->admin_model->getUserDetails();

		// file creation 
		$file = fopen('php://output', 'w');

		$header = array("ID","Email","Registered at"); 
		fputcsv($file, $header);
		foreach ($usersData as $key=>$line){ 
		 fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}

	public function Delete() {
        $id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
            $uid = $this->input->post("id");
            $table = $this->input->post("table");
            $st = $this->admin_model->delete($uid, $table);
            print_r($st);
        }
    }

    public function SaveSlides()
    {
    	$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->index();
        } else {
			if($_FILES['slide']['name'] != ''){
				$target_dir = "uploads/slides/";
				$target_file = $target_dir .time()."_".basename($_FILES['slide']['name']);
				$name=time()."_".basename($_FILES['slide']['name']);
				move_uploaded_file($_FILES["slide"]["tmp_name"], $target_file);
			}else{
				$name = $this->input->post("slide_val");
			}
			
            $data = array(
						"slide" => $name,
						"text" => $this->input->post("text"),
						"slide_text" => str_replace("?","",$this->input->post("slide_text")),
					);
			$res = $this->admin_model->insert(TABLE_PREFIX."_slides",$data);
			if($res){
				redirect(site_url("admin/Slides"));
			}else{
				redirect(site_url("admin/Slides"));
			}
        }
    }

	public function not_found()
	{
		$this->load->view("404");
	}

	//logout
    public function logout() {
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('a_fname');
		$this->session->unset_userdata('a_lname');
		$this->session->unset_userdata('a_email');
		$this->session->unset_userdata('a_mobile');
		$this->session->unset_userdata('a_role');
		$this->session->unset_userdata('a_status');
	redirect(site_url("admin"));
    }
}