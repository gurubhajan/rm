<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct() {
        parent::__construct();
        // $this->load->helper('search');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');
		$this->load->library('pagination');
        $this->load->library('session');
        $this->load->model("user_model");
    }
	
    public function headers()
    {
  		// $data = "*";
		// $where = Null;
		// $res = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,$where);
		// $result['trek_users'] = $res->row();

		$this->load->view("user_views/header");
		// $this->load->view("user_views/header",$result);
    }

    public function index()
    {	
    	$message = array("class" => "", "msg" => "");
        $id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->load->view('user_views/login',$message);
        } else {
            $this->dashboard();
        }
    }

    public function register($message=NULL)
	{
		if($message == NULL){
			$message = array("class" => "", "msg" => "");
		}
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->load->view('user_views/register',$message);
        } else {
            $this->dashboard();
        }
	}
	public function reg_user()
	{
		$rss = $this->user_model->check_user($this->input->post('email'));
		//echo '<pre>'.print_r($rss->email).'</pre>';
		if(!empty($rss->email)){
			$message = array("class" => "danger", "msg" => "It seems your email is already in our records");
			echo json_encode($message);
		} else {
			$data = array(
						"firstname" => $this->input->post('firstname'),
						"lastname" => $this->input->post('lastname'),
						"email" => $this->input->post('email'),
						"mobile" => $this->input->post('phone'),
						"password" => $this->input->post('password'),
						"role" => 1,
						"status" => 0,
					);
			$request = $this->user_model->insert(TABLE_PREFIX."_users",$data);
			$message = array("class" => "success", "msg" => " Registration successfully completed, Please login now!!");
			echo json_encode($message);
		}
	}

    public function login($message=NULL)
	{
		if($message==NULL){
			$message = array("class" => "", "msg" => "");
		}
		$id = $this->session->userdata("admin_id");
        if ($id == "") {
            $this->load->view('user_views/login',$message);
        } else {
            $this->dashboard();
        }
	}

    public function sign_in()
    {
        $rss = $this->user_model->check_user($this->input->post('email'), $this->input->post('password'));
		//echo '<pre>'.print_r($rss).'</pre>';
		if(!empty($rss->email)){
			// if($rss->status==0){
			// 	$message = array("class" => "danger", "msg" => "You are not an Admin User");
	  //           $this->login($message);
			// }else{
				$res = $this->user_model->login_user($this->input->post('email'), $this->input->post('password'));
				if (!empty($res)) {
					redirect(site_url("user/dashboard"));
				} else {
					$message = array("class" => "danger", "msg" => "Invalid login credentials");
					$this->login($message);
				}
			// }
		} else {
			$message = array("class" => "danger", "msg" => "It seems your email isn't in our records");
	        $this->login($message);
		}
    }

    public function dashboard()
	{	
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
			$data = "*";
			//$result['users_count'] = $this->user_model->selectRecord(TABLE_PREFIX."_certificates",'count(id) as all_users',NULL)->row()->all_users;
        	$result['tickets'] = $this->user_model->selectRecord(TABLE_PREFIX."_ticket",$data,array('user_id' => $id))->result();
			$result['get_all_users'] = $this->user_model->selectRecordbyDesc(TABLE_PREFIX."_users", $data, array('role'=>0))->result();
			$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
			$result['dash'] = "selected";
			$this->headers();
			$this->load->view('user_views/sidebar', $result);
			$this->load->view('user_views/dashboard', $result);
			$this->load->view("user_views/footer");
		}
	}

	public function manage_profile()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/manage-profile', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function campaign()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/new-campaign', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function campaign_list()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/campaign-list', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function running_campaign()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/running-campaign', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function schedule_campaign()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/schedule-campaign', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function data_generator()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/data-generator', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function buy_credit()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$result['credit_list'] = $this->user_model->selectRecord(TABLE_PREFIX."_package_credit_list",$data,NULL)->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/buy-credit', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function transactions()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
			$result['transactions'] = $this->user_model->selectRecord(TABLE_PREFIX."_transaction",$data,array('user_id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/transactions', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function create_ticket()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/create-ticket', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function ticket_list()
	{
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = "*";
        	$result['tickets'] = $this->user_model->selectRecord(TABLE_PREFIX."_ticket",$data,array('user_id' => $id))->result();
        	$result['u_credit'] = $this->user_model->selectRecord(TABLE_PREFIX."_user_credits",$data,array('id'=>$id))->row();
        	$result['c_user'] = $this->user_model->selectRecord(TABLE_PREFIX."_users",$data,array('id' => $id))->result();
        	$this->headers();
			$this->load->view('user_views/sidebar', $result);
        	$this->load->view('user_views/ticket-list', $result);
        	$this->load->view("user_views/footer");
        }
	}

	public function reg_ticket(){
		$id = $this->session->userdata("user_id");
        if ($id == "") {
            $this->index();
        } else {
        	$data = array(
				"user_id" => $id,
				"user_email" => $this->session->userdata("u_email"),
				"subject" => $this->input->post('subject'),
				"department" => $this->input->post('department'),
				"message" => $this->input->post('message'),
				"status" => 'pending',
			);
			$request = $this->user_model->insert(TABLE_PREFIX."_ticket",$data);
			if($request){
				$message = array("class" => "success", "msg" => "Ticket created successfully.");
				echo json_encode($message);
			}else{
				$message = array("class" => "warning", "msg" => "Failed to create ticket.");
				echo json_encode($message);
			}
        }
	}

	public function getNumberSeries(){
		$state = $_POST['state'];
		$operator = $_POST['operator'];
		
		$res = $this->user_model->selectRecord(TABLE_PREFIX.'_number_generator','*',array('state' => $state, 'operator' => $operator))->result();
		
		if(!empty($res)){
			echo "<option value=''>--Select--</option>";
			foreach($res as $k=>$v){
				echo "<option value='".$v->series."'>".$v->series."</option>";
			}
		}else{
			echo 2;
		}
	}
	//Campaigns Data
	public function createCampaign(){
		date_default_timezone_set('Asia/Kolkata');
		
		$user_id = $this->session->userdata('user_id');
		
		$credits = $this->user_model->selectRecord(TABLE_PREFIX.'_user_credits','*',array('user_id' => $user_id))->row()->available_credit;
		
		if($credits<$_POST['quantity']){
			echo false;
		}else{
			$data = array(
						'number_of_data' => $_POST['quantity'],
						'status' => 0,
						'created_at' => date('Y-m-d H:i:s')
					);
			
			$ins_id = $this->user_model->insert(TABLE_PREFIX."_campaigns", $data);
			
			if($ins_id>0){
				for($i=1;$i<=$_POST['quantity'];$i++){
					$six_number = sprintf("%06d", mt_rand(1, 999999));
					$number = '91'.$_POST['series'].$six_number;
					$data = array(
								'campaign_id' => $ins_id,
								'number' => $number,
								'name' => 'Pending',
								'whatsapp' => 'Pending',
								'operator' => $_POST['operator'],
								'circle' => $_POST['state'],
								'credit_used' => 1,
								'created_at' => date('Y-m-d H:i:s')
							);
					
					$this->user_model->insert(TABLE_PREFIX."_campaigns_data", $data);
				}
				$balance = $credits-$_POST['quantity'];
				$this->user_model->update(TABLE_PREFIX.'_user_credits', array('available_credit' => $balance), array('user_id' => $user_id));
				echo true;
			}
		}
	}

	public function scheduleCampaign(){
		date_default_timezone_set('Asia/Kolkata');
<<<<<<< HEAD
		
=======
>>>>>>> ee88083961fc024827943d0ffbd7bda59b23dd76
		$user_id = $this->session->userdata('user_id');
		
		$credits = $this->user_model->selectRecord(TABLE_PREFIX.'_user_credits','*',array('user_id' => $user_id))->row()->available_credit;
		
		if($credits<$_POST['quantity']){
			echo false;
		}else{
			$data = array(
						'number_of_data' => $_POST['quantity'],
						'status' => 0,
						'created_at' => date('Y-m-d H:i:s', strtotime($_POST['date_time']))
					);
			
			$ins_id = $this->user_model->insert(TABLE_PREFIX."_campaigns", $data);
			
			if($ins_id>0){
				for($i=1;$i<=$_POST['quantity'];$i++){
					$six_number = sprintf("%06d", mt_rand(1, 999999));
					$number = '91'.$_POST['series'].$six_number;
					$data = array(
								'campaign_id' => $ins_id,
								'number' => $number,
								'name' => 'Pending',
								'whatsapp' => 'Pending',
								'operator' => $_POST['operator'],
								'circle' => $_POST['state'],
								'credit_used' => 1,
								'created_at' => date('Y-m-d H:i:s', strtotime($_POST['date_time']))
							);
					
					$this->user_model->insert(TABLE_PREFIX."_campaigns_data", $data);
				}
				$balance = $credits-$_POST['quantity'];
				$this->user_model->update(TABLE_PREFIX.'_user_credits', array('available_credit' => $balance), array('user_id' => $user_id));
				echo true;
			}
		}
	}

	public function not_found()
	{
		$this->load->view("404");
	}

	//logout
    public function logout() {
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('u_fname');
		$this->session->unset_userdata('u_lname');
		$this->session->unset_userdata('u_email');
		$this->session->unset_userdata('u_mobile');
		$this->session->unset_userdata('u_role');
		$this->session->unset_userdata('u_status');
		$this->session->unset_userdata('u_state');
		$this->session->unset_userdata('u_city');
		$this->session->unset_userdata('u_address');
		$this->session->unset_userdata('u_pincode');
	redirect(site_url("/"));
    }
}