<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() {
        parent::__construct();
        //$this->load->helper('search');
        $this->load->helper('url');
        $this->load->library('form_validation');
		$this->load->library('pagination');
        $this->load->model("welcome_model");
        $this->load->library('session');
        // Load file helper
        $this->load->helper('file');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = "*";
		$result['site_details'] = $this->welcome_model->selectRecord(TABLE_PREFIX."_user_details", $data, array('admin_id' => "1" ))->row();
		$this->load->view('coming_soon/index', $result);
	}

	public function validate_certificate()
	{
		$this->load->view('validate-certificate');
	}

	public function view_certificate()
	{
		$cid = $this->input->get('cid', TRUE );
		//die($cid);
		$data = "*";
		$result['cert_details'] = $this->welcome_model->selectRecord(TABLE_PREFIX."_certificates", $data, array('certificate_id' => $cid ))->row();
		$this->load->view('view-certificate', $result);
		
	}

	public function view_certificate_card()
	{
		$cid = $this->input->get('cid', TRUE );
		//die($cid);
		$data = "*";
		$result['cert_details'] = $this->welcome_model->selectRecord(TABLE_PREFIX."_certificates", $data, array('certificate_id' => $cid ))->row();
		$this->load->view('view-certificate-card', $result);
		
	}
}
