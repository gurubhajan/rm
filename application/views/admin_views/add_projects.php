<?php
	if($data =="add"){
		$url = site_url("admin/saveCertificate");
	}else{
		$url = site_url("admin/update_slides");
		$data = $data->row();
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb bg-white">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
			<h5 class="font-medium text-uppercase mb-0">Projects > <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Project</h5>
		</div>
		<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
			<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
				<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
					<li class="breadcrumb-item"><a href="#">Projects</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Project</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="page-content container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-uppercase mb-0"> <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Project </h5>
				</div>
				<div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <form class="form-horizontal form-material" action="<?php echo $url ?>" enctype="multipart/form-data" method="POST">
                            <div class="form-group">
                                <label for="proj_name" class="col-md-12">Name</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->proj_name; } ?>" class="form-control form-control-line" name="proj_name" id="proj_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="proj_address" class="col-md-12">Address</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->proj_address; } ?>" class="form-control form-control-line" name="proj_address" id="proj_address" >		                                        
                                </div>
                            </div>
                            <div class="col-md-12" style="display:inline-flex;">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="proj_img_1" class="col-sm-5 control-label">1st Image</label>
                                        <div class="col-sm-8 dp-blok">
                                            <input type="file" name="proj_img_1" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
                                        
                                            <?php if($data != "add"){ if($data->image!=NULL){ ?>
                                                <br/>
                                                <br/>
                                                <input type="hidden" name="proj_img_1_val" value="<?php echo $data->proj_img_1; ?>" class="form-control" >
                                                <img width="100%" src="<?php echo site_url('uploads/project_img').'/'.$data->proj_img_1; ?>" />
                                            
                                            <?php } } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="proj_img_2" class="col-sm-5 control-label">2nd Image</label>
                                        <div class="col-sm-8 dp-blok">
                                            <input type="file" name="proj_img_2" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
                                        
                                            <?php if($data != "add"){ if($data->image!=NULL){ ?>
                                                <br/>
                                                <br/>
                                                <input type="hidden" name="proj_img_2_val" value="<?php echo $data->proj_img_2; ?>" class="form-control" >
                                                <img width="100%" src="<?php echo site_url('uploads/project_img').'/'.$data->proj_img_2; ?>" />
                                            
                                            <?php } } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gross_weight" class="col-md-12">Project Type</label>
                                <div class="col-md-12">
                                    <select name="proj_type" class="form-control form-control-line">
                                    <?php if($data == "add"){ ?>
                                        <option value="" selected disabled>Select Type</option>
                                    <?php } ?>
                                        <option value="Architetural" <?php echo ($data->type == "Architetural")?'selected':''; ?>>Architetural</option>
                                        <option value="Construction" <?php echo ($data->type == "Construction")?'selected':''; ?>>Construction</option>
                                        <option value="Interior" <?php echo ($data->type == "Interior")?'selected':''; ?>>Interior</option>
                                    </select>
                                	<!-- <input type="text" value="<?php if($data != "add"){ echo $data->gross_weight; } ?>" class="form-control form-control-line" name="gross_weight" id="gross_weight" >-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shape" class="col-md-12">Status</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->shape; } ?>" class="form-control form-control-line" name="shape" id="shape">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="additional_notes" class="col-md-12">Additional Notes</label>
                                <div class="col-md-12">
                                	<textarea rows="8" style="border:1px solid #edeff0;" name="additional_notes" id="additional_notes" class="form-control form-control-line"><?php if($data != "add"){ echo $data->additional_notes; } ?></textarea>
                                	<!-- <input type="text" value="" class="form-control form-control-line" name="additional_notes" id="additional_notes" >-->
                                </div>
                            </div>
                            
							<!-- <div class="box-body">
								<div class="form-group">
									<label for="signature_img" class="col-sm-2 control-label">Autorised Signature</label>
									<div class="col-sm-5 dp-blok">
										<input type="file" name="signature_img" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
									
										<?php if($data != "add"){ if($data->image!=NULL){ ?>
											<br/>
											<br/>
											<input type="hidden" name="signature_img_val" value="<?php echo $data->signature_img; ?>" class="form-control" >
											<img width="100%" src="<?php echo site_url('uploads/signature_img').'/'.$data->signature_img; ?>" />
										
										<?php } } ?>
									</div>
								</div>
							</div> -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                	<input type="hidden" name="id" value="<?php if($data != "add"){ echo $data->id; } ?>" />
                                    <input type="submit" class="btn btn-success" style="float: right !important;" id="btn" value="<?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Project" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>