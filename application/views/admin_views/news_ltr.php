				<div class="page-breadcrumb bg-white">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
							<h5 class="font-medium text-uppercase mb-0">Newsletter Subscribers</h5>
						</div>
						<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
							<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
								<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
									<li class="breadcrumb-item"><a href="<?php echo base_url('admin') ?>">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Newsletter Subscribers</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="page-content container-fluid">
					<!-- ============================================================== -->
					<!-- User Table & Profile Cards Section  -->
					<!-- ============================================================== -->
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title text-uppercase mb-0"> Subscribers</h5> 
									<a href='<?php base_url() ?>exportCSV' style="float:right;" class="btn"><button class="btn btn-primary">Export Data&nbsp;<i class="mdi mdi-download"></i></button></a>
								</div>
								<div class="table-responsive">
									<table class="table no-wrap user-table mb-0">
										<thead>
											<tr>
												<th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
												<th scope="col" class="border-0 text-uppercase font-medium">Email</th>
												<th scope="col" class="border-0 text-uppercase font-medium">Registered On</th>
												<!-- <th scope="col" class="border-0 text-uppercase font-medium">Manage</th> -->
											</tr>
										</thead>
										<tbody>
											<?php
												$i = 1; 
												//echo '<pre>'.print_r($get_all_emails).'</pre>';
												foreach($get_all_emails as $k => $v){ 
												
												?>
											 <tr>
												<td class="pl-4"><?php echo $i; ?></td>
												<td>
													<span class="text-muted"><a href="mailto:<?php echo $v->email; ?>"><?php echo $v->email; ?></a></span><br>
													<!-- <span class="text-muted">999 - 444 - 555</span> -->
												</td>
												<td>
													<span class="text-muted"><?php echo $newDate = date("d-M-Y", strtotime($v->reg_at)); ?></span>
													<!-- <button type="button" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i> </button>
													<button type="button" class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2"><i class="ti-pencil-alt"></i> </button> -->
												</td>
											</tr>
											<?php
												$i++;
												}
											?>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
						    $("select#user_status").change(function(){
						    	if (confirm("Are you sure !")) {
							        //var array = $('#searchKeywords').val().split(",");
							        var ch_status = $(this).children("option:selected").val();
							        //alert("You have selected the status - " + ch_status);
						        	$.ajax({
									    url: '<?php echo site_url() ?>admin/change_stat',
									    type: 'post',
									    //dataType: 'json',
									    data: { "user_stat": ch_status},
									    success: function(data) {
									    	//alert(data);
									    	console.log(data);
									    	if (data == 1) {
									            alert("Status updated successfully!!");
									            location.reload();
										   	} else if (data == 0){
										   		alert("Some error occured, try later...");
										   		location.reload();
									        }
									    }
									});
								
						    	}
						    	return false;
						    });
						});
					</script>