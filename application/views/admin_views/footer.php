					<script>
						function Delete(id,table,type,url){
							if (confirm("Are you sure delete this "+type)) {
								$.ajax({
									url:"<?php echo site_url("admin/Delete");?>",
									type:"POST",
									data:{"id":id,"table":table},
									success:function(result){
										if(result){
											alert("Deleted Sucessfully");
											window.location.href="<?php echo site_url("admin/")?>"+url;
										}
									}
								});
							}
							return false;
						 }
					</script>

					<!-- ============================================================== -->
					<!-- footer -->
					<!-- ============================================================== -->
					<footer class="footer text-center">
						All Rights Reserved by © <?php echo TITLE; ?>. Designed and Developed by
						<a href="https://www.fiverr.com/gurubhajan" target="_blank">Guru Bhajan Singh</a>.
					</footer>
					<!-- ============================================================== -->
					<!-- End footer -->
					<!-- ============================================================== -->
				</div>
				<!-- ============================================================== -->
				<!-- End Page wrapper  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Wrapper -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- customizer Panel -->
			<!-- ============================================================== -->
			
			<div class="chat-windows"></div>
			<!-- Bootstrap tether Core JavaScript -->
			<script src="<?php echo base_url() ?>assets/admin/libs/popper.js/dist/umd/popper.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/libs/bootstrap/dist/js/bootstrap.min.js"></script>
			<!-- apps -->
			<script src="<?php echo base_url() ?>assets/admin/dist/js/app.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/dist/js/app.init.minimal.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/dist/js/app-style-switcher.js"></script>
			<!-- slimscrollbar scrollbar JavaScript -->
			<script src="<?php echo base_url() ?>assets/admin/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/extra-libs/sparkline/sparkline.js"></script>
			<!--Wave Effects -->
			<script src="<?php echo base_url() ?>assets/admin/dist/js/waves.js"></script>
			<!--Menu sidebar -->
			<script src="<?php echo base_url() ?>assets/admin/dist/js/sidebarmenu.js"></script>
			<!--Custom JavaScript -->
			<script src="<?php echo base_url() ?>assets/admin/dist/js/custom.min.js"></script>
			<!-- This Page JS -->
			<script src="<?php echo base_url() ?>assets/admin/libs/chartist/dist/chartist.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/dist/js/pages/chartist/chartist-plugin-tooltip.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/extra-libs/c3/d3.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/extra-libs/c3/c3.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/libs/raphael/raphael.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/libs/morris.js/morris.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/dist/js/pages/dashboards/dashboard1.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/libs/moment/min/moment.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/libs/fullcalendar/dist/fullcalendar.min.js"></script>
			<script src="<?php echo base_url() ?>assets/admin/dist/js/pages/calendar/cal-init.js"></script>
			
			<script>
				$('#calendar').fullCalendar('option', 'height', 650);
			</script>
		</div>
	</body>
</html>