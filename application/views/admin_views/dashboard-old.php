	<div class="page-breadcrumb bg-white">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
				<h5 class="font-medium text-uppercase mb-0">Dashboard</h5>
			</div>
			<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
				<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
					<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
						<li class="breadcrumb-item"><a href="<?php echo base_url('admin') ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="page-content container-fluid">
		<!-- ============================================================== -->
		<!-- Card Group  -->
		<!-- ============================================================== -->
		<div class="card-group">
			 <div class="card p-2 p-lg-3">
				<div class="p-lg-3 p-2">
					<div class="d-flex align-items-center">
						<button class="btn btn-circle btn-cyan text-white btn-lg" href="javascript:void(0)">
						<i class="ti-wallet"></i>
						</button>
						<div class="ml-4" style="width: 38%;">
							<h4 class="font-light">Total Users</h4>
							<div class="progress">
								<div class="progress-bar bg-cyan" role="progressbar" style="width: <?php if(empty($users_count)){ echo '1'; }else{ echo $users_count;} ?>%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="40"></div>
							</div>
						</div>
						<div class="ml-auto">
							<h2 class="display-7 mb-0"><?php echo $users_count; ?></h2>
						</div>
					</div>
				</div>
			</div> 
			<div class="card p-2 p-lg-3" style="background-color: #edf1f5; border-right: 0px;">
				<!--	<div class="p-lg-3 p-2">
					<div class="d-flex align-items-center">
						<button class="btn btn-circle btn-danger text-white btn-lg" href="javascript:void(0)">
						<i class="ti-clipboard"></i>
						</button>
						<div class="ml-4" style="width: 38%;">
							<h4 class="font-light">Total Projects</h4>
							<div class="progress">
								<div class="progress-bar bg-danger" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="40"></div>
							</div>
						</div>
						<div class="ml-auto">
							<h2 class="display-7 mb-0">23</h2>
						</div>
					</div>
				</div>-->
			</div>
			<div class="card p-2 p-lg-3" style="background-color: #edf1f5; border-right: 0px;">
			<!-- 	<div class="p-lg-3 p-2">
					<div class="d-flex align-items-center">
						<button class="btn btn-circle btn-warning text-white btn-lg" href="javascript:void(0)">
						<i class="fas fa-dollar-sign"></i>
						</button>
						<div class="ml-4" style="width: 38%;">
							<h4 class="font-light">Total Earnings</h4>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="40"></div>
							</div>
						</div>
						<div class="ml-auto">
							<h2 class="display-7 mb-0">83</h2>
						</div>
					</div>
				</div>-->
			</div> 
		</div>
		<!-- ============================================================== -->
		<!-- User Table & Profile Cards Section  -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title text-uppercase mb-0">Manage Users</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Name</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Email</th>
									<th scope="col" class="border-0 text-uppercase font-medium">User Status</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Create On</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Manage</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 1; 
									foreach($get_all_users as $k => $v){ 
									//echo '<pre>'.print_r($k).'</pre>';
									?>
								 <tr>
									<td class="pl-4"><?php echo $i; ?></td>
									<td>
										<h5 class="font-medium mb-0"><?php echo $v->name; ?></h5>
										<!-- <span class="text-muted">Texas, Unitedd states</span> -->
									</td>
									<td>
										<span class="text-muted"><a href="mailto:<?php echo $v->email; ?>"><?php echo $v->email; ?></a></span><br>
										<!-- <span class="text-muted">999 - 444 - 555</span> -->
									</td>
									<!-- <td>
										<span class="text-muted">Visual Designer</span><br>
										<span class="text-muted">Past : teacher</span>
									</td> -->
									<td>
										<select class="form-control category-select" id="user_status">
											<option <?php if($v->status == 1){ echo 'selected="selected"';  } ?> value="1,<?php echo $v->id; ?>" >Active</option>
											<option <?php if($v->status == 0){ echo 'selected="selected"';  } ?> value="0,<?php echo $v->id; ?>" >Inactive</option>
										</select>
									</td>
									<td>
										<span class="text-muted"><?php echo $newDate = date("d-M-Y", strtotime($v->registration_date)); ?></span>
										<!-- <button type="button" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i> </button>
										<button type="button" class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2"><i class="ti-pencil-alt"></i> </button> -->
									</td>
									<td>
										<a href="javascript:Delete('<?php echo $v->id;?>','<?php echo TABLE_PREFIX; ?>_users','User','')" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i></a>
									</td>
								</tr>
								<?php
									$i++;
									}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
		    $("select#user_status").change(function(){
		    	if (confirm("Are you sure !")) {
			        //var array = $('#searchKeywords').val().split(",");
			        var ch_status = $(this).children("option:selected").val();
			        //alert("You have selected the status - " + ch_status);
		        	$.ajax({
					    url: '<?php echo site_url() ?>admin/change_stat',
					    type: 'post',
					    //dataType: 'json',
					    data: { "user_stat": ch_status},
					    success: function(data) {
					    	//alert(data);
					    	console.log(data);
					    	if (data == 1) {
					            alert("Status updated successfully!!");
					            location.reload();
						   	} else if (data == 0){
						   		alert("Some error occured, try later...");
						   		location.reload();
					        }
					    }
					});
				
		    	}
		    	return false;
		    });
		});
	</script>
</div>