			<div class="page-breadcrumb bg-white">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
						<h5 class="font-medium text-uppercase mb-0">Slide > All Slides</h5>
					</div>
					<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
						<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
							<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
								<li class="breadcrumb-item"><a href="#">Slide</a></li>
								<li class="breadcrumb-item active" aria-current="page">All Slides</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="page-content container-fluid">
				<!-- <div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title text-uppercase mb-0">Edit About Us Page </h5>
							</div>
							<div class="col-md-12 col-xs-12">
								<div class="white-box">
									<form class="form-horizontal form-material" id="about_us" method="POST">
										<div class="form-group">
											<?php
				                                // echo '<pre>';
					                                // print_r($row);
												// echo '</pre>';
											?>
											<label class="col-md-12">Page Url:</label>
											<div class="col-md-12">
												<input type="text" value="<?php echo $row['url']; ?>" class="form-control form-control-line" name="pg_url" id="pg_url" readonly="readonly">
												<input type="hidden" name="pg_id" id="pg_id" value="<?php echo $row['id']; ?>">
											</div>
										</div>
										<div class="form-group">
											<label for="pg_title" class="col-md-12">Title Text</label>
											<div class="col-md-12">
												<input type="text" placeholder="Title" class="form-control form-control-line" name="pg_title" id="pg_title" value="<?php echo $row['title']; ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-12">Message</label>
											<div class="col-md-12">
												<textarea rows="25" name="editor1" id="pg_content" class="form-control form-control-line"><?php echo $row['content']; ?></textarea>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<button class="btn btn-success" style="float: right !important;" id="btn">Update Page</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title text-uppercase mb-0">All Slides</h5>
							</div>
							<div class="table-responsive">
								<table class="table no-wrap user-table mb-0">
									<thead>
										<tr>
											<th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
											<!-- <th scope="col" class="border-0 text-uppercase font-medium">ID</th> -->
											<th scope="col" class="border-0 text-uppercase font-medium">Title</th>
											<th scope="col" class="border-0 text-uppercase font-medium">Text</th>
											<th scope="col" class="border-0 text-uppercase font-medium">Image</th>
											<th scope="col" class="border-0 text-uppercase font-medium">Create On</th>
											<th scope="col" class="border-0 text-uppercase font-medium">Manage</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$i = 1; 
											foreach($get_all_slides as $k => $v){ 
											//echo '<pre>'.print_r($k).'</pre>';
											?>
										 <tr>
											<td class="pl-4"><?php echo $i; ?></td>
											<td >
												<h5 class="font-medium mb-0" ><?php echo substr($v->title,0, 25)." . . ."; ?></h5>
												<!-- <span class="text-muted">Texas, Unitedd states</span> -->
											</td>
											<td >
												<h5 class="font-medium mb-0" ><?php echo substr($v->text,0, 25)." . . ."; ?></h5>
												<!-- <span class="text-muted">Texas, Unitedd states</span> -->
											</td>
											<td>
												<span class="text-muted sp_img"><img src="<?php echo site_url().'uploads/slides/'.$v->image; ?>" alt=" " /></span><br>
												<!-- <span class="text-muted">999 - 444 - 555</span> -->
											</td>
											<td>
												<span class="text-muted"><?php echo $v->created_on; ?></span><br>
											</td>
											<td>
												<a href="javascript:DeleteSlider('<?php echo $v->id;?>','<?php echo TABLE_PREFIX; ?>_slides','Slider')" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i> </a>
												<a href="<?php echo site_url("admin/edit_slider").'/'.$v->id ?>" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-pencil-alt"></i> </a>
												<!-- <button type="button" id="edit" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i>  -->
												<!-- <button type="button" class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2"><i class="ti-pencil-alt"></i> </button> -->
											</td>
										</tr>
										<?php
											$i++;
											}
										?>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				function DeleteSlider(id,table,type){
					if (confirm("Are you sure delete this "+type)) {
						$.ajax({
							url:"<?php echo site_url(); ?>admin/Delete",
							type:"post",
							data:{id:id,table:table},
							success:function(result){
								if(result){
									alert("Deleted Sucessfully");
									window.location.href="<?php echo site_url() ?>"+"admin/all_slides/";
								}
							}
						});
					}
					return false;
				}
			</script>
