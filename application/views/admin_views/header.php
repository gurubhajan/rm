<!DOCTYPE html>
<html dir="ltr" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/admin/images/favicon.png">
		<title><?php echo TITLE; ?> | Admin</title>
		<!-- chartist CSS -->
		<link href="<?php echo base_url() ?>assets/admin/libs/chartist/dist/chartist.min.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/admin/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css" rel="stylesheet">
		<!--c3 CSS -->
		<link href="<?php echo base_url() ?>assets/admin/libs/morris.js/morris.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/admin/extra-libs/c3/c3.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="<?php echo base_url() ?>assets/admin/libs/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
		<link href="<?php echo base_url() ?>assets/admin/extra-libs/calendar/calendar.css" rel="stylesheet" />
		<!-- needed css -->
		<link href="<?php echo base_url() ?>assets/admin/dist/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/admin/dist/css/style.min.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/admin/dist/css/custom.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!-- [if lt IE 9]-->
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<!--[endif]-->
		<!-- ============================================================== -->
		<!-- All Jquery -->
		<!-- ============================================================== -->
		<script src="<?php echo base_url() ?>assets/admin/libs/jquery/dist/jquery.min.js"></script>
		<script src="https://ckeditor.com/latest/ckeditor.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/libs/ckeditor/core_editor.js"></script>

		<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" rel="stylesheet">
		<!-- Page level plugin JavaScript-->
		<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

	</head>
	<body>
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<div class="preloader">
			<div class="lds-ripple">
				<div class="lds-pos"></div>
				<div class="lds-pos"></div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- Main wrapper - style you can find in pages.scss -->
		<!-- ============================================================== -->
		<div id="main-wrapper">
			<!-- ============================================================== -->
			<!-- Topbar header - style you can find in pages.scss -->
			<!-- ============================================================== -->
		<header class="topbar">
		    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
		        <div class="navbar-header">
		            <!-- This is for the sidebar toggle which is visible on mobile only -->
		            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
		            <a class="navbar-brand" href="<?php echo base_url('admin') ?>">
		                <!-- Logo icon -->
		                <b class="logo-icon">
		                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
		                    <!-- Dark Logo icon -->
		                    <img src="<?php echo base_url() ?>assets/admin/images/logos/logo-icon.png" alt="homepage" class="dark-logo" />
		                    <!-- Light Logo icon -->
		                    <img src="<?php echo base_url() ?>assets/admin/images/logos/logo-light-icon.png" alt="homepage" class="light-logo" />
		                </b>
		                <!--End Logo icon -->
		                <!-- Logo text -->
		                <span class="logo-text">
		                     <!-- dark Logo text -->
		                     <img src="<?php echo base_url() ?>assets/admin/images/logos/logo-text.png" alt="homepage" class="dark-logo" />
		                     <!-- Light Logo text -->   
		                     <img src="<?php echo base_url() ?>assets/admin/images/logos/logo-light-text.png" class="light-logo" alt="homepage" />
		                </span>
		            </a>
		            <!-- ============================================================== -->
		            <!-- End Logo -->
		            <!-- ============================================================== -->
		            <!-- ============================================================== -->
		            <!-- Toggle which is visible on mobile only -->
		            <!-- ============================================================== -->
		            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
		        </div>
		        <!-- ============================================================== -->
		        <!-- End Logo -->
		        <!-- ============================================================== -->
		        <div class="navbar-collapse collapse" id="navbarSupportedContent">
		            <!-- ============================================================== -->
		            <!-- toggle and nav items -->
		            <!-- ============================================================== -->
		            <ul class="navbar-nav float-left mr-auto">
		                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-18"></i></a></li>
		                <!-- ============================================================== -->
		                <!-- Messages -->
		                <!-- ============================================================== -->
		                
		                <!-- ============================================================== -->
		                <!-- Comment -->
		                <!-- ============================================================== -->
		                
		                <!-- ============================================================== -->
		                <!-- End mega menu -->
		                <!-- ============================================================== -->
		            </ul>
		            <!-- ============================================================== -->
		            <!-- Right side toggle and nav items -->
		            <!-- ============================================================== -->
		            <ul class="navbar-nav float-right">
		                <!-- ============================================================== -->
		                <!-- Search -->
		                <!-- ============================================================== -->
		                <li class="nav-item search-box">
		                    <!-- <form class="app-search d-none d-lg-block">
		                        <input type="text" class="form-control" placeholder="Search...">
		                        <a href="#" class="active"><i class="fa fa-search"></i></a>
		                    </form> -->
		                </li>
		                <!-- ============================================================== -->
		                <!-- User profile and search -->
		                <!-- ============================================================== -->
		                <li class="nav-item dropdown">
		                    <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                        <img src="<?php echo base_url(); ?>assets/admin/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
		                        <span class="ml-2 user-text font-medium"><?php echo $this->session->userdata("a_name"); ?></span><span class="fas fa-angle-down ml-2 user-text"></span>
		                    </a>
		                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
		                        <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
		                            <div class="">
		                            	<img src="<?php echo base_url(); ?>assets/admin/images/users/1.jpg" alt="user" class="rounded" width="80">
		                            </div>
		                            <div class="ml-2">
		                                <h4 class="mb-0"><?php echo $this->session->userdata("a_name"); ?></h4>
		                                <p class=" mb-0 text-muted"><?php echo $this->session->userdata("a_email"); ?></p>
		                                <!-- <a href="javascript:void(0)" class="btn btn-sm btn-danger text-white mt-2 btn-rounded">View Profile</a> -->
		                            </div>
		                        </div>
		                        <?php if($this->session->userdata("a_write") == 1){ ?>
		                        <a class="dropdown-item" href="<?php echo base_url('admin/edit_profile'); ?>"><i class="ti-user mr-1 ml-1"></i> Edit Profile</a>
		                        <?php } ?>
		                        <!--<a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet mr-1 ml-1"></i> My Balance</a>
		                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email mr-1 ml-1"></i> Inbox</a>
		                        <div class="dropdown-divider"></div>
		                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings mr-1 ml-1"></i> Account Setting</a>
		                        <div class="dropdown-divider"></div> -->
		                        <!-- <a class="dropdown-item" href="https://www.queenslander.com/" target="_blank"><i class="fa fa-share-square mr-1 ml-1"></i> Customer View </a> -->
		                        <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
		                    </div>
		                </li>
		                <!-- ============================================================== -->
		                <!-- User profile and search -->
		                <!-- ============================================================== -->
		            </ul>
		        </div>
		    </nav>
		</header>
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->