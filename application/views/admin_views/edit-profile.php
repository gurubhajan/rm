				<div class="page-breadcrumb bg-white">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
							<h5 class="font-medium text-uppercase mb-0">Edit Profile</h5>
						</div>
						<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
							<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
								<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
									<li class="breadcrumb-item"><a href="<?php echo base_url('admin') ?>">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="page-content container-fluid">
					<!-- ============================================================== -->
					<!-- Edit Profile Section  -->
					<!-- ============================================================== -->
					<div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg">
                            	<img width="100%" alt="user" src="<?php echo base_url() ?>assets/admin/images/background/img1.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                        	<img src="<?php echo base_url() ?>assets/admin/images/users/1.jpg" class="thumb-lg img-circle" alt="img">
                                        </a>
                                        <h4 class="text-white"><?php echo $this->session->userdata("a_name") ?></h4>
                                        <h5 class="text-white"><?php echo $this->session->userdata("a_email") ?></h5></div>
                                </div>
                            </div>
                            <!-- <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                    <h1>258</h1>
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue"><i class="ti-twitter"></i></p>
                                    <h1>125</h1>
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger"><i class="ti-dribbble"></i></p>
                                    <h1>556</h1>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Full Name" value="<?php echo $this->session->userdata("a_name") ?>" name="full_name" class="form-control form-control-line" readonly />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="Your Email" value="<?php echo $this->session->userdata("a_email") ?>" class="form-control form-control-line" name="user_email" id="euser_email" readonly />
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" value="password" class="form-control form-control-line"> </div>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label class="col-md-12">Phone No (for Users)</label>
                                    <div class="col-md-12">
                                        <input type="tel" placeholder="" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Email (for Users)</label>
                                    <div class="col-md-12">-->
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                     <!--   <input type="email" placeholder="" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Select Country</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line">
                                            <option>London</option>
                                            <option>India</option>
                                            <option>Usa</option>
                                            <option>Canada</option>
                                            <option>Thailand</option>
                                        </select>
                                    </div>
                                </div> -->
                                <!-- <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div> -->
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <?php if($msg!=""){ ?>
                            <!-- <div id="message" style="color:red;text-align: center;"> -->
                            <div class="alert alert-<?php echo $class; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-<?php echo $class=="danger"?'ban':'check' ?>"></i> <?php echo $class=="danger"?'Alert':'Success' ?>!</h4>
                            <?php  print_R($msg); ?>
                            </div>
                            <?php } ?>
                            <form class="form-horizontal form-material" action="" id="update_ainfo" method="POST">
                                <div class="form-group">
                                    <label class="col-md-12">Phone No (for Users)</label>
                                    
                                    <div class="col-md-12">
                                        <input type="tel" placeholder="" value="<?php echo $user_pdata[0]->phone; ?>" id="p_phno" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Email (for Users)</label>
                                    <div class="col-md-12">
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                        <input type="email" placeholder="" value="<?php echo $user_pdata[0]->sec_email; ?>" id="p_email" class="form-control form-control-line" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Address (for Users)</label>
                                    <div class="col-md-12">
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                        <input type="text" placeholder="" value="<?php echo $user_pdata[0]->address; ?>" id="p_address" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Facebook Url (for Users)</label>
                                    <div class="col-md-12">
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                        <input type="text" placeholder="" value="<?php echo $user_pdata[0]->f_url; ?>" id="f_url" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Twitter Url (for Users)</label>
                                    <div class="col-md-12">
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                        <input type="text" placeholder="" value="<?php echo $user_pdata[0]->t_url; ?>" id="t_url" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Linkedin Url (for Users)</label>
                                    <div class="col-md-12">
                                        <!-- <textarea rows="5" class="form-control form-control-line"></textarea> -->
                                        <input type="text" placeholder="" value="<?php echo $user_pdata[0]->l_url; ?>" id="l_url" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
				<script type="text/javascript">
                    $(document).ready(function(){
                        $( "form#update_ainfo" ).submit(function( event ) {
                            event.preventDefault();
                            var p_phno = $("#p_phno").val();
                            var p_email = $("#p_email").val();
                            var p_address = $("#p_address").val();
                            var f_url = $("#f_url").val();
                            var t_url = $("#t_url").val();
                            var l_url = $("#l_url").val();
                            //var pg_content = CKEDITOR.instances.pg_content.getData();
                            // //var pg_content = CKEDITOR.instances.editor1.getData();
                            //alert( pg_content );
                            //var editorData= CKEDITOR.instances["editor1"].getData();
                      //    alert(" your data is :"+editorData);

                            if( p_email != "" ){
                                $.ajax({
                                    url:'<?php echo site_url(); ?>admin/uadmin_info',
                                    type:'post',
                                    data:{p_phno:p_phno, p_email:p_email, p_address:p_address, f_url:f_url, t_url:t_url, l_url:l_url},
                                    success:function(data){
                                        //var msg = "";
                                        // alert(data);
                                        // if(data == true){
                                        //     alert("Successfully updated...");
                                        //     location.reload();
                                        // }else if(data == false){
                                        //     //alert("Some error occured, try later...");
                                        //     location.reload();
                                        // }
                                    }
                                });
                            }else{
                                alert("Email field is required. This can't be empty.");
                            }
                        });
                    });            
                </script>