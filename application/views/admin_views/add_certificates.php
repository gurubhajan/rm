<?php
	if($data =="add"){
		$url = site_url("admin/saveCertificate");
	}else{
		$url = site_url("admin/update_slides");
		$data = $data->row();
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb bg-white">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
			<h5 class="font-medium text-uppercase mb-0">Certificates > <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Certificate</h5>
		</div>
		<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
			<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
				<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
					<li class="breadcrumb-item"><a href="#">Certificates</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Certificate</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="page-content container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-uppercase mb-0"> <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Certificate </h5>
				</div>
				<div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <form class="form-horizontal form-material" action="<?php echo $url ?>" enctype="multipart/form-data" method="POST">
                            <div class="form-group">
                                <label for="description" class="col-md-12">Description</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->description; } ?>" class="form-control form-control-line" name="description" id="description" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="diamond_weight" class="col-md-12">Diamond Weight</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->diamond_weight; } ?>" class="form-control form-control-line" name="diamond_weight" id="diamond_weight" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gross_weight" class="col-md-12">Gross Weight</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->gross_weight; } ?>" class="form-control form-control-line" name="gross_weight" id="gross_weight" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shape" class="col-md-12">Shape</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->shape; } ?>" class="form-control form-control-line" name="shape" id="shape" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="finish" class="col-md-12">Finish</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->finish; } ?>" class="form-control form-control-line" name="finish" id="finish" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="colour" class="col-md-12">Colour</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->colour; } ?>" class="form-control form-control-line" name="colour" id="colour" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clarity" class="col-md-12">Clarity</label>
                                <div class="col-md-12">
                                	<input type="text" value="<?php if($data != "add"){ echo $data->clarity; } ?>" class="form-control form-control-line" name="clarity" id="clarity" >		                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="additional_notes" class="col-md-12">Additional Notes</label>
                                <div class="col-md-12">
                                	<textarea rows="3" name="additional_notes" id="additional_notes" class="form-control form-control-line"><?php if($data != "add"){ echo $data->additional_notes; } ?></textarea>
                                	<!-- <input type="text" value="" class="form-control form-control-line" name="additional_notes" id="additional_notes" >-->
                                </div>
                            </div>
                            <div class="box-body">
								<div class="form-group">
									<label for="product_img" class="col-sm-2 control-label">Image of True Likeness</label>
									<div class="col-sm-5 dp-blok">
										<input type="file" name="product_img" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
									
										<?php if($data != "add"){ if($data->image!=NULL){ ?>
											<br/>
											<br/>
											<input type="hidden" name="product_img_val" value="<?php echo $data->product_img; ?>" class="form-control" >
											<img width="100%" src="<?php echo site_url('uploads/product_img').'/'.$data->product_img; ?>" />
										
										<?php } } ?>
									</div>
								</div>
							</div>
							<div class="box-body">
								<div class="form-group">
									<label for="signature_img" class="col-sm-2 control-label">Autorised Signature</label>
									<div class="col-sm-5 dp-blok">
										<input type="file" name="signature_img" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
									
										<?php if($data != "add"){ if($data->image!=NULL){ ?>
											<br/>
											<br/>
											<input type="hidden" name="signature_img_val" value="<?php echo $data->signature_img; ?>" class="form-control" >
											<img width="100%" src="<?php echo site_url('uploads/signature_img').'/'.$data->signature_img; ?>" />
										
										<?php } } ?>
									</div>
								</div>
							</div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                	<input type="hidden" name="id" value="<?php if($data != "add"){ echo $data->id; } ?>" />
                                    <input type="submit" class="btn btn-success" style="float: right !important;" id="btn" value="<?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Certificate" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>