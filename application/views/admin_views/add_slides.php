				<?php
					
					if($data =="add"){
						$url = site_url("admin/save_slides");
					}else{
						$url = site_url("admin/update_slides");
						$data = $data->row();
					}
				?>
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<div class="page-breadcrumb bg-white">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
							<h5 class="font-medium text-uppercase mb-0">Sliders > <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Slider</h5>
						</div>
						<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
							<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
								<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
									<li class="breadcrumb-item"><a href="#">Sliders</a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Slider</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="page-content container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title text-uppercase mb-0"> <?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Slider </h5>
								</div>
								<div class="col-md-12 col-xs-12">
			                        <div class="white-box">
			                            <form class="form-horizontal form-material" action="<?php echo $url ?>" enctype="multipart/form-data" method="POST">
			                                <!-- <div class="form-group">
			                                	<?php 
			                                		// echo '<pre>';
			                                		// print_r($row);
			                                		// echo '</pre>';
			                                	?>
			                                    <label class="col-md-12">Slider Text</label>
			                                    <div class="col-md-12">
			                                    	<input type="text" name="title" class="form-control" id="inputName3" value="<?php// echo $data->title ?>" placeholder="Title">
			                                        <input type="text" value="" class="form-control form-control-line" name="pg_url" id="pg_url" readonly="readonly">
			                                        <input type="hidden" name="pg_id" id="pg_id" value="">
			                                    </div>
			                                </div> -->
			                                <div class="form-group">
			                                    <label for="slider_title" class="col-md-12">Slider Title</label>
			                                    <div class="col-md-12">
			                                    	<input type="text" value="<?php if($data != "add"){ echo $data->title; } ?>" class="form-control form-control-line" name="slider_title" id="slider_title" >
			                                    	<!-- <textarea rows="25" name="slider_text" id="slider_text" class="form-control form-control-line"></textarea> -->
			                                        
			                                    </div>
			                                </div>
			                                <div class="form-group">
			                                    <label for="slider_text" class="col-md-12">Slider Text</label>
			                                    <div class="col-md-12">
			                                    	<input type="text" value="<?php if($data != "add"){ echo $data->text; } ?>" class="form-control form-control-line" name="slider_text" id="slider_text" >
			                                    	<!-- <textarea rows="25" name="slider_text" id="slider_text" class="form-control form-control-line"></textarea> -->
			                                        
			                                    </div>
			                                </div>
			                                <div class="box-body">
												<div class="form-group">
													<label for="slide" class="col-sm-2 control-label">Slider Image</label>
													<div class="col-sm-5 dp-blok">
														<input type="file" name="slider" class="form-control" <?php if($data=="add"){ echo 'required';} ?> />
													
													<?php if($data != "add"){ if($data->image!=NULL){ ?>
														<br/>
														<br/>
														<input type="hidden" name="slide_val" value="<?php echo $data->image; ?>" class="form-control" >
														<img width="100%" src="<?php echo site_url('uploads/slides').'/'.$data->image; ?>" />
													
													<?php } } ?>
												</div>
											</div>
			                                <!-- <div class="form-group">
			                                    <label class="col-md-12">Content 01</label>
			                                    <div class="col-md-12">
			                                        <textarea rows="25" name="editor00" id="pg_content" class="form-control form-control-line"></textarea>
			                                    </div>
			                                </div> -->
			                                <!-- <div class="form-group">
			                                    <label class="col-md-12">Content 02</label>
			                                    <div class="col-md-12">
			                                        <textarea rows="25" name="editor02" id="pg_cont_1" class="form-control form-control-line"></textarea>
			                                    </div>
			                                </div>
			                                <div class="form-group">
			                                    <label class="col-md-12">Content 03</label>
			                                    <div class="col-md-12">
			                                        <textarea rows="25" name="editor03" id="pg_cont_2" class="form-control form-control-line"></textarea>
			                                    </div>
			                                </div>
			                                <div class="form-group">
			                                    <label class="col-md-12">Content 04</label>
			                                    <div class="col-md-12">
			                                        <textarea rows="25" name="editor04" id="pg_cont_3" class="form-control form-control-line"></textarea>
			                                    </div>
			                                </div> -->
			                                <div class="form-group">
			                                    <div class="col-sm-12">
			                                    	<input type="hidden" name="id" value="<?php if($data != "add"){ echo $data->id; } ?>"></input>
			                                        <input type="submit" class="btn btn-success" style="float: right !important;" id="btn" value="<?php if($data=="add"){ echo 'Add';}else {echo 'Update';} ?> Slider" />
			                                    </div>
			                                </div>
			                            </form>
			                        </div>
			                    </div>
							</div>
						</div>
						<script>
			                    //CKEDITOR.replace( 'slider_text' );
			                    // var pg_content = CKEDITOR.instances.pg_content.getData();
			            </script>
					