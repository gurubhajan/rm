<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile text-center dropdown p-3">
                        <div class="user-pic"><img src="<?php echo base_url() ?>assets/admin/images/users/1.jpg" alt="users" class="rounded-circle" width="50" /></div>
                        <div class="user-content hide-menu">
                            <a href="javascript:void(0)" class="mt-2" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h5 class="mb-0 user-name mt-2"><?php echo $this->session->userdata("a_name") ?> <i class="fa fa-angle-down"></i></h5>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="Userdd">
                                <?php if($this->session->userdata("a_write") == 1){ ?>
                                <a class="dropdown-item" href="<?php echo base_url('admin/edit_profile'); ?>"><i class="ti-user m-r-5 m-l-5"></i> Edit Profile</a>
                                <?php } ?>
                                <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>
                <li class="sidebar-item <?php if(!empty($dash)){ echo $dash; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin') ?>" aria-expanded="false">
                        <i class="mdi mdi-av-timer"></i>
                        <span class="hide-menu"> Dashboard </span>
                    </a>
                </li>
                <div class="devider"></div>
                <li class="sidebar-item <?php if(!empty($setdefset)){ echo $setdefset; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/default-setting" aria-expanded="false">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="hide-menu"> Set Default Settings </span>
                    </a>
                </li>
                <div class="devider"></div>
                <li class="sidebar-item <?php if(!empty($subscribers)){ echo $subscribers; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/users" aria-expanded="false">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="hide-menu"> Subscribers </span>
                    </a>
                </li>
                <div class="devider"></div>
                <li class="sidebar-item <?php if(!empty($set_pack_credit)){ echo $set_pack_credit; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/packagecredit" aria-expanded="false">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="hide-menu"> Set Package Credit </span>
                    </a>
                </li>
                <div class="devider"></div>
                <li class="sidebar-item <?php if(!empty($notification_prof)){ echo $notification_prof; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/notificationprofile" aria-expanded="false">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="hide-menu"> Notification Profile </span>
                    </a>
                </li>
                <div class="devider"></div>
                <li class="sidebar-item <?php if(!empty($utickets)){ echo $utickets; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/tickets" aria-expanded="false">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="hide-menu"> User Tickets </span>
                    </a>
                </li>
                <!-- <div class="devider"></div>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-presentation-play"></i>
                        <span class="hide-menu"> Slider </span>
                        <span class="badge badge-warning text-white badge-pill ml-auto mr-3 font-medium px-2 py-1"></span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item " <?php if(!empty($alslid)){ echo $alslid; } ?> >
                            <a href="<?php echo base_url() ?>admin/all_slides" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">All Sliders</span>
                            </a>
                        </li>
                        <li class="sidebar-item " <?php if(!empty($adslid)){ echo $adslid; } ?> >
                            <a href="<?php echo base_url() ?>admin/add_slider" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">Add Slider</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <!-- <div class="devider"></div>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-file-document"></i>
                        <span class="hide-menu"> Certificates </span>
                        <span class="badge badge-warning text-white badge-pill ml-auto mr-3 font-medium px-2 py-1"></span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item " <?php if(!empty($alcert)){ echo $alcert; } ?> >
                            <a href="<?php echo base_url() ?>admin/allCertificates" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">All Certificates</span>
                            </a>
                        </li>
                        <li class="sidebar-item " <?php if(!empty($adcert)){ echo $adcert; } ?> >
                            <a href="<?php echo base_url() ?>admin/addCertificate" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">Add Certificate</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <div class="devider"></div>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-file-document"></i>
                        <span class="hide-menu"> Projects </span>
                        <span class="badge badge-warning text-white badge-pill ml-auto mr-3 font-medium px-2 py-1"></span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item " <?php if(!empty($alprjcts)){ echo $alprjcts; } ?> >
                            <a href="<?php echo base_url() ?>admin/AllProjects" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">All Projects</span>
                            </a>
                        </li>
                        <li class="sidebar-item " <?php if(!empty($adprjcts)){ echo $adprjcts; } ?> >
                            <a href="<?php echo base_url() ?>admin/AddProject" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">Add Project</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="sidebar-item <?php if(!empty($nwsltr)){ echo $nwsltr; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/newsletter_emails" aria-expanded="false">
                        <i class="mdi mdi-email-outline"></i>
                        <span class="hide-menu"> Newsletter Email List </span>
                    </a>
                </li> -->

                <!-- <li class="sidebar-item <?php if(!empty($glry_mg)){ echo $glry_mg; } ?>">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin/gallery_images" aria-expanded="false">
                        <i class="mdi mdi-message-text-outline"></i>
                        <span class="hide-menu"> Gallery </span>
                    </a>
                </li> -->
                <!-- <div class="devider"></div> -->
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-content-copy"></i>
                        <span class="hide-menu"> Pages </span>
                        <span class="badge badge-warning text-white badge-pill ml-auto mr-3 font-medium px-2 py-1">06</span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="edit-home.php" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">Home</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="edit-about_us.php" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu">About Us</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-email-open-outline"></i>
                                <span class="hide-menu">Email Templates</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="email-templete-alert.html" class="sidebar-link">
                                        <i class="mdi mdi-message-alert"></i>
                                        <span class="hide-menu"> Alert </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="email-templete-basic.html" class="sidebar-link">
                                        <i class="mdi mdi-message-bulleted"></i>
                                        <span class="hide-menu"> Basic</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="email-templete-billing.html" class="sidebar-link">
                                        <i class="mdi mdi-message-draw"></i>
                                        <span class="hide-menu"> Billing</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="email-templete-password-reset.html" class="sidebar-link">
                                        <i class="mdi mdi-message-bulleted-off"></i>
                                        <span class="hide-menu"> Password-Reset</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-account-circle"></i>
                                <span class="hide-menu">Authentication</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="authentication-login1.html" class="sidebar-link">
                                        <i class="mdi mdi-account-key"></i>
                                        <span class="hide-menu"> Login </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="authentication-login2.html" class="sidebar-link">
                                        <i class="mdi mdi-account-key"></i>
                                        <span class="hide-menu"> Login 2 </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="authentication-register1.html" class="sidebar-link">
                                        <i class="mdi mdi-account-plus"></i>
                                        <span class="hide-menu"> Register</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="authentication-register2.html" class="sidebar-link">
                                        <i class="mdi mdi-account-plus"></i>
                                        <span class="hide-menu"> Register 2</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="authentication-lockscreen.html" class="sidebar-link">
                                        <i class="mdi mdi-account-off"></i>
                                        <span class="hide-menu"> Lockscreen</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="authentication-recover-password.html" class="sidebar-link">
                                        <i class="mdi mdi-account-convert"></i>
                                        <span class="hide-menu"> Recover password</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-alert-box"></i>
                                <span class="hide-menu">Error Pages</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="error-400.html" class="sidebar-link">
                                        <i class="mdi mdi-alert-outline"></i>
                                        <span class="hide-menu"> Error 400 </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="error-403.html" class="sidebar-link">
                                        <i class="mdi mdi-alert-outline"></i>
                                        <span class="hide-menu"> Error 403</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="error-404.html" class="sidebar-link">
                                        <i class="mdi mdi-alert-outline"></i>
                                        <span class="hide-menu"> Error 404</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="error-500.html" class="sidebar-link">
                                        <i class="mdi mdi-alert-outline"></i>
                                        <span class="hide-menu"> Error 500</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="error-503.html" class="sidebar-link">
                                        <i class="mdi mdi-alert-outline"></i>
                                        <span class="hide-menu"> Error 503</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-animation.html" class="sidebar-link">
                                <i class="mdi mdi-debug-step-over"></i>
                                <span class="hide-menu">Animation</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-search-result.html" class="sidebar-link">
                                <i class="mdi mdi-search-web"></i>
                                <span class="hide-menu">Search Result</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-gallery.html" class="sidebar-link">
                                <i class="mdi mdi-camera-iris"></i>
                                <span class="hide-menu">Gallery</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-treeview.html" class="sidebar-link">
                                <i class="mdi mdi-file-tree"></i>
                                <span class="hide-menu">Treeview</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-block-ui.html" class="sidebar-link">
                                <i class="mdi mdi-codepen"></i>
                                <span class="hide-menu">Block UI</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-session-timeout.html" class="sidebar-link">
                                <i class="mdi mdi-timer-off"></i>
                                <span class="hide-menu">Session Timeout</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-session-idle-timeout.html" class="sidebar-link">
                                <i class="mdi mdi-timer-sand-empty"></i>
                                <span class="hide-menu">Session Idle Timeout</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-utility-classes.html" class="sidebar-link">
                                <i class="mdi mdi-tune"></i>
                                <span class="hide-menu">Helper Classes</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="pages-maintenance.html" class="sidebar-link">
                                <i class="mdi mdi-camera-iris"></i>
                                <span class="hide-menu">Maintenance Page</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-cart-outline"></i>
                        <span class="hide-menu">Ecommerce</span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="eco-products.html" class="sidebar-link">
                                <i class="mdi mdi-cards-variant"></i>
                                <span class="hide-menu">Products</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="eco-products-cart.html" class="sidebar-link">
                                <i class="mdi mdi-cart"></i>
                                <span class="hide-menu">Products Cart</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="eco-products-edit.html" class="sidebar-link">
                                <i class="mdi mdi-cart-plus"></i>
                                <span class="hide-menu">Products Edit</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="eco-products-detail.html" class="sidebar-link">
                                <i class="mdi mdi-camera-burst"></i>
                                <span class="hide-menu">Product Details</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="eco-products-orders.html" class="sidebar-link">
                                <i class="mdi mdi-chart-pie"></i>
                                <span class="hide-menu">Product Orders</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="eco-products-checkout.html" class="sidebar-link">
                                <i class="mdi mdi-clipboard-check"></i>
                                <span class="hide-menu">Products Checkout</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-format-color-fill"></i>
                        <span class="hide-menu">Ui Elements </span>
                        <span class="badge badge-info badge-pill ml-auto mr-3 font-medium px-2 py-1">12</span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="ui-buttons.html" class="sidebar-link">
                                <i class="mdi mdi-toggle-switch"></i>
                                <span class="hide-menu"> Buttons</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-modals.html" class="sidebar-link">
                                <i class="mdi mdi-tablet"></i>
                                <span class="hide-menu"> Modals</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-tab.html" class="sidebar-link">
                                <i class="mdi mdi-sort-variant"></i>
                                <span class="hide-menu"> Tab</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-tooltip-popover.html" class="sidebar-link">
                                <i class="mdi mdi-image-filter-vintage"></i>
                                <span class="hide-menu"> Tooltip &amp; Popover</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-notification.html" class="sidebar-link">
                                <i class="mdi mdi-message-bulleted"></i>
                                <span class="hide-menu"> Notification</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-progressbar.html" class="sidebar-link">
                                <i class="mdi mdi-poll"></i>
                                <span class="hide-menu"> Progressbar</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-typography.html" class="sidebar-link">
                                <i class="mdi mdi-format-line-spacing"></i>
                                <span class="hide-menu"> Typography</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-bootstrap.html" class="sidebar-link">
                                <i class="mdi mdi-bootstrap"></i>
                                <span class="hide-menu"> Bootstrap Ui</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-breadcrumb.html" class="sidebar-link">
                                <i class="mdi mdi-equal"></i>
                                <span class="hide-menu"> Breadcrumb</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-list-media.html" class="sidebar-link">
                                <i class="mdi mdi-file-video"></i>
                                <span class="hide-menu"> List Media</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-grid.html" class="sidebar-link">
                                <i class="mdi mdi-view-module"></i>
                                <span class="hide-menu"> Grid</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-carousel.html" class="sidebar-link">
                                <i class="mdi mdi-view-carousel"></i>
                                <span class="hide-menu"> Carousel</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-scrollspy.html" class="sidebar-link">
                                <i class="mdi mdi-crop-free"></i>
                                <span class="hide-menu"> Scrollspy</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-spinner.html" class="sidebar-link">
                                <i class="mdi mdi-application"></i>
                                <span class="hide-menu"> Spinner</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="ui-toasts.html" class="sidebar-link">
                                <i class="mdi mdi-apple-safari"></i>
                                <span class="hide-menu"> Toasts</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-apps"></i>
                        <span class="hide-menu">Apps</span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="app-chats.html" class="sidebar-link">
                                <i class="mdi mdi-comment-processing-outline"></i>
                                <span class="hide-menu">Chat Message</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-inbox-arrow-down"></i>
                                <span class="hide-menu">Inbox</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="inbox-email.html" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> Email </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="inbox-email-detail.html" class="sidebar-link">
                                        <i class="mdi mdi-email-alert"></i>
                                        <span class="hide-menu"> Email Detail </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="inbox-email-compose.html" class="sidebar-link">
                                        <i class="mdi mdi-email-secure"></i>
                                        <span class="hide-menu"> Email Compose </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="ti-user"></i>
                                <span class="hide-menu">Contacts</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="contact-list.html" class="sidebar-link">
                                        <i class="icon-people"></i>
                                        <span class="hide-menu"> Contact List </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="contact-grid.html" class="sidebar-link">
                                        <i class="icon-user-follow"></i>
                                        <span class="hide-menu"> Contacts Grid </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-bookmark-plus-outline"></i>
                                <span class="hide-menu">Tickets</span>
                            </a>
                            <ul aria-expanded="false" class="collapse second-level">
                                <li class="sidebar-item">
                                    <a href="ticket-list.html" class="sidebar-link">
                                        <i class="mdi mdi-book-multiple"></i>
                                        <span class="hide-menu"> Ticket List </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="ticket-detail.html" class="sidebar-link">
                                        <i class="mdi mdi-book-plus"></i>
                                        <span class="hide-menu"> Ticket Detail </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a href="app-taskboard.html" class="sidebar-link">
                                <i class="mdi mdi-bulletin-board"></i>
                                <span class="hide-menu"> Taskboard </span>
                            </a>
                        </li>
                    </ul>
                </li> -->                
                <div class="devider"></div>
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="https://wrappixel.com/ampleadmin/docs/documentation.html" aria-expanded="false">
                        <i class="mdi mdi-adjust text-danger"></i>
                        <span class="hide-menu">Documentation</span>
                    </a>
                </li> -->
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/logout'); ?>" aria-expanded="false">
                        <i class="mdi mdi-adjust text-info"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                </li>
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="pages-faq.html" aria-expanded="false">
                        <i class="mdi mdi-adjust text-success"></i>
                        <span class="hide-menu">FAQs</span>
                    </a>
                </li> -->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>