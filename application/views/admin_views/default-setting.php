<div class="page-breadcrumb bg-white">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
            <h5 class="font-medium text-uppercase mb-0">Set Default Settings</h5>
        </div>
        <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
            <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                <ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Set Default Settings</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="page-content container-fluid">
    <!-- ============================================================== -->
    <!-- Set Default Settings Section  -->
    <!-- ============================================================== -->
    <div class="row">
    
        <div class="col-md-12 col-xs-12">
            <div class="white-box">
                <div class="alert alert-dismissible" id="msg" role="alert" style="display:none;">

                </div>
                <form class="form-horizontal form-material" action="" id="udefault_setting" method="POST">
                    <div class="form-group">
                        <label class="col-md-12">Default Credit</label>
                        <div class="col-md-6">
                            <input type="text" placeholder="" value="<?php echo $default_setting->default_credit; ?>" id="default_credit" class="form-control form-control-line">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Price Per Credit</label>
                        <div class="col-md-6">
                            <input type="text" placeholder="" value="<?php echo $default_setting->price_per_credit; ?>" id="price_per_credit" class="form-control form-control-line">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-success">Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $( "form#udefault_setting" ).submit(function( event ) {
                event.preventDefault();
                var d_credit = $("#default_credit").val();
                var pp_credit = $("#price_per_credit").val();

                if( d_credit != "" && pp_credit != ""){
                    $.ajax({
                        url:'<?php echo site_url(); ?>admin/update_dsetting',
                        type:'post',
                        dataType : 'json',
                        data:{d_credit:d_credit, pp_credit:pp_credit},
                        success:function(response){
                            $("#msg").removeClass( "alert-success" );
                            $("#msg").removeClass( "alert-danger" );
                            $("#msg").empty();
                            if(response.class == "success") {
                                $("#msg").addClass( "alert-success" );
                                var btn = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                                var h4div = '<h4><i class="icon fa fa-check"></i> Success! </h4>';
                                $("#msg").append(btn,h4div,response.msg);
                                $("#msg").show();
                            } else {
                                $("#msg").addClass( "alert-danger" );
                                var btn = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                                var h4div = '<h4><i class="icon fa fa-times"></i> Alert! </h4>';
                                $("#msg").append(btn,h4div,response.msg);
                                $("#msg").show();
                            }

                            setTimeout(function(){
                                $("#msg").hide();
                            },5000);
                        } 
                    });
                }else{
                    alert("'Default credit' and 'Price Per credit' both fields are required!");
                }
            });
        });            
    </script>
</div>