<div class="page-breadcrumb bg-white">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
				<h5 class="font-medium text-uppercase mb-0">Package Credit List</h5>
			</div>
			<div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
				<nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
					<ol class="breadcrumb mb-0 justify-content-end p-0 bg-white">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active" aria-current="page">Package Credit List</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="page-content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card container">
					<div class="card-body">
						<h5 class="card-title text-uppercase mb-0">Package Credit List</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0" id="dataTable">
							<thead>
								<tr>
									<th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">No Of Credit</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Price Per Credit</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Date</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 1; 
									foreach($package_credit_list as $k => $v){ 
									//echo '<pre>'.print_r($v).'</pre>';
									?>
								 <tr>
									<td class="pl-4"><?php echo $i; ?></td>
									<td >
										<h5 class="font-medium mb-0" ><?php echo $v->no_of_credit; ?></h5>
									</td>
									<td >
										<h5 class="font-medium mb-0" ><?php echo $v->price_per_credit; ?></h5>
									</td>
                                    <td >
										<h5 class="font-medium mb-0" ><?php echo $v->date; ?></h5>
									</td>
									<td>
										 <a href="javascript:DeleteProject('<?php echo $v->id;?>','<?php echo TABLE_PREFIX; ?>_projects','Project')" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-trash"></i> </a>
										<!--<a href="<?php echo site_url("admin/edit_slider").'/'.$v->id ?>" class="btn btn-outline-info btn-circle btn-lg btn-circle"><i class="ti-pencil-alt"></i> </a>
										<a href="/view-Project/?cid=<?php echo $v->Project_id; ?>" target="_blank"><button class="btn btn-outline-info btn-lg">View Project</button></a> -->
									</td>
									<!-- <td>
										<a href="/view-Project-card/?cid=<?php echo $v->Project_id; ?>" target="_blank"><button class="btn btn-outline-info btn-lg">View Project</button></a>
									</td> -->
								</tr>
								<?php
									$i++;
									}
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
		    $('#dataTable').DataTable();
		});
	</script>
	<script>
		function DeleteProject(id,table,type){
			if (confirm("Are you sure delete this "+type)) {
				$.ajax({
					url:"<?php echo site_url(); ?>admin/Delete",
					type:"post",
					data:{id:id,table:table},
					success:function(result){
						if(result){
							alert("Deleted Sucessfully");
							window.location.href="<?php echo site_url() ?>"+"admin/all_slides/";
						}
					}
				});
			}
			return false;
		}
	</script>
</div>
