<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Whatsapp Dash</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/user/images/favicon.ico">
    <!-- Bootstrap Css -->
    <link href="<?php echo site_url(); ?>assets/user/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo site_url(); ?>assets/user/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo site_url(); ?>assets/user/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden neu">
                        <div class="bg-soft-primary">
                            <div class="row">
                                <div class="col-7">
                                    <div class="text-primary p-4">
                                        <h5 class="text-primary">Free Register</h5>
                                        <p>Get your free Mjay account now.</p>
                                    </div>
                                </div>
                                <div class="col-5 align-self-end">
                                    <img src="<?php echo site_url(); ?>assets/user/images/profile-img.png" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div>
                                <a href="<?php echo site_url(); ?>">
                                    <div class="avatar-md profile-user-wid mb-4">
                                        <span class="avatar-title rounded-circle bg-light text-dark">
                                            <img src="<?php echo site_url(); ?>assets/user/images/logo.svg" alt="" class="rounded-circle" height="34">
                                        </span>
                                    </div>
                                </a>
                            </div>
                            <div class="p-2">        
                                <div class="alert" role="alert" id="msg" style="display:none;">
                                    
                                </div>
                                <form class="form-horizontal" action="reg-user" id="register">
                                    <div class="form-group">
                                        <label for="Name">First Name</label>
                                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Enter First Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Last Name</label>
                                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter last name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Mobile Number</label>
                                        <input type="tel" name="phone" class="form-control" id="phone" placeholder="Enter mobile number">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                                    </div>
                                    <div class="mt-4">
                                        <button class="btn btn-primary btn-block waves-effect waves-light btn-effect" type="submit">Register</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 text-center">
                        <div>
                            <p>Have an account ? <a href="<?php echo site_url(); ?>user/login" class="font-weight-medium text-primary"> Sign In </a> </p>
                            <p>© 2020 Mjay. Crafted with <i class="mdi mdi-heart text-danger"></i> by Manjay Gupta</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- JAVASCRIPT -->
    <script src="<?php echo site_url(); ?>assets/user/libs/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/simplebar/simplebar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/node-waves/waves.min.js"></script>
    <!-- App js -->
    <script src="<?php echo site_url(); ?>assets/user/js/app.js"></script>
    <script type="text/javascript">
        $('form#register').on('submit', function (e) {
            
            e.preventDefault();

            $.ajax({
                type: 'post',
                dataType: "json",
                url: 'user/reg_user',
                data: $('form#register').serialize(),
                success: function (response) {
                    // console.log(response);
                    // console.log("Hi");
                    $("#msg").removeClass( "alert-success" );
                    $("#msg").removeClass( "alert-danger" );
                        if (response.class == "success" ){
                            //setTimeout(function(){
                                $("#msg").addClass( "alert-success" );
                                $("#msg").text(response.msg);
                                $("#msg").show();
                            //},4000);    
                            
                        } else {
                            //setTimeout(function(){
                                $("#msg").addClass( "alert-danger" );
                                $("#msg").text(response.msg);
                                $("#msg").show();
                            //},4000);
                        }
                    
                    setTimeout(function(){
                        $("#msg").hide();
                    },5000);
                }
            });
        });
    </script>
</body>
</html>