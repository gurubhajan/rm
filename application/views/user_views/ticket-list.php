<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Transaction History</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="col-md-12">
                <div class="card neu">
                    <div class="card-body">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr class="tb-head">
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th> Subject </th>
                                    <th> Department </th>
                                    <th> Date </th>
                                    <th> Status </th>
                                    <th> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach($tickets as $key => $val){ ?>
                                <?php //echo '<pre>'.print_r($val).'</pre>'; ?>
                                <tr class="tb-row">
                                    <td><?= $i ?></td>
                                    <td><?php echo $c_user[0]->firstname.' '.$c_user[0]->lastname; ?></td>
                                    <td><?= $val->user_email ?></td>
                                    <td><?= $val->subject ?></td>
                                    <td><?php echo ($val->department == 2)?"Billing":"Support"; ?></td>
                                    <td><?= $val->created_at ?></td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect"><?= $val->status ?></span> </td>
                                    <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                </tr>
                                <?php $i++; } ?>
                                <!-- <tr class="tb-row">
                                    <td>1</td>
                                    <td>John Doe</td>
                                    <td>john@doe.com</td>
                                    <td>Check Demo</td>
                                    <td>Support</td>
                                    <td>02-12-2020 01:26:51</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>John Doe</td>
                                    <td>john@doe.com</td>
                                    <td>Check Demo</td>
                                    <td>Support</td>
                                    <td>02-12-2020 01:26:51</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>John Doe</td>
                                    <td>john@doe.com</td>
                                    <td>Check Demo</td>
                                    <td>Support</td>
                                    <td>02-12-2020 01:26:51</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>John Doe</td>
                                    <td>john@doe.com</td>
                                    <td>Check Demo</td>
                                    <td>Support</td>
                                    <td>02-12-2020 01:26:51</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->