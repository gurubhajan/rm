			<footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>
                            document.write(new Date().getFullYear())
                            </script> © Manjay Gupta.
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->
    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>
    <!-- JAVASCRIPT -->
    <script src="<?php echo site_url(); ?>assets/user/libs/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/simplebar/simplebar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/node-waves/waves.min.js"></script>
    <!-- Required datatable js -->
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/jszip/jszip.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/user/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Datatable init js -->
    <script src="<?php echo site_url(); ?>assets/user/js/pages/datatables.init.js"></script>
    <!-- apexcharts -->
    <script src="<?php echo site_url(); ?>assets/user/libs/apexcharts/apexcharts.min.js"></script>
    <!-- apexcharts init -->
    <script src="<?php echo site_url(); ?>assets/user/js/pages/apexcharts.init.js"></script>
    <!-- App js -->
    <script src="<?php echo site_url(); ?>assets/user/js/app.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>