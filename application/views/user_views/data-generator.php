<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
	<div class="page-content">
		<div class="container-fluid">
			<!-- start page title -->
			<div class="row">
				<div class="col-12">
					<div class="page-title-box d-flex align-items-center justify-content-between">
						<h4 class="mb-0 font-size-18">Data Generator</h4>
					</div>
				</div>
			</div>
			<!-- end page title -->
			<div class="row">
				<div class="col-lg-12">
					<div class="card neu">
						<div class="card-body">
							<form id="form" method="POST" action="<?php echo site_url('user/createCampaign') ?>">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-lg-8">
										<div class="form-group">
											<label for="formrow-inputState">State</label>
											<select id="formrow-inputState" name="state" class="form-control" required onchange="changeState(this.value)">
												<option value="">--Choose State--</option>
												<option value="Andhra Pradesh">Andhra Pradesh</option>
												<option value="Assam">Assam</option>
												<option value="Bihar">Bihar</option>
												<option value="Chennai">Chennai</option>
												<option value="Delhi">Delhi</option>
												<option value="Gujarat">Gujarat</option>
												<option value="Haryana">Haryana</option>
												<option value="Himachal Pradesh">Himachal Pradesh</option>
												<option value="Jammu and Kashmir">Jammu and Kashmir</option>
												<option value="Karnataka">Karnataka</option>
												<option value="Kerala">Kerala</option>
												<option value="Kolkata">Kolkata</option>
												<option value="Madhya Pradesh">Madhya Pradesh</option>
												<option value="Maharashtra &amp; Goa">Maharashtra &amp; Goa</option>
												<option value="Mumbai">Mumbai</option>
												<option value="North East">North East</option>
												<option value="Orissa">Orissa</option>
												<option value="Punjab">Punjab</option>
												<option value="Rajasthan">Rajasthan</option>
												<option value="Tamil Nadu">Tamil Nadu</option>
												<option value="Uttar Pradesh (East)">Uttar Pradesh (East)</option>
												<option value="Uttar Pradesh (West)">Uttar Pradesh (West)</option>
												<option value="West Bengal">West Bengal</option>
											</select>
										</div>
										<div class="form-group">
											<label for="formrow-inputOperator">Operator</label>
											<select id="formrow-inputOperator" name="operator" class="form-control" required onchange="changeOperator(this.value)">
												<option value="">--Select--</option>
												<option value="Aircel">Aircel</option>
												<option value="Airtel">Airtel</option>
												<option value="BSNL - CDMA">BSNL - CDMA</option>
												<option value="BSNL - CellOne GSM">BSNL - CellOne GSM</option>
												<option value="BSNL - GSM">BSNL - GSM</option>
												<option value="DOLPHIN">DOLPHIN</option>
												<option value="Etisalat DB Telecom India">Etisalat DB Telecom India</option>
												<option value="Idea">Idea</option>
												<option value="Loop Mobile">Loop Mobile</option>
												<option value="MTS">MTS</option>
												<option value="PING CDMA">PING CDMA</option>
												<option value="Reliance - CDMA">Reliance - CDMA</option>
												<option value="Reliance - GSM">Reliance - GSM</option>
												<option value="Reliance Jio">Reliance Jio</option>
												<option value="S Tel Ltd">S Tel Ltd</option>
												<option value="Sistema Shyam TeleServices Ltd. (CDMA) (MTS)">Sistema Shyam TeleServices Ltd. (CDMA) (MTS)</option>
												<option value="Spice Communications Ltd.">Spice Communications Ltd.</option>
												<option value="Subrin Rintel">Subrin Rintel</option>
												<option value="Tata DoCoMo - CDMA">Tata DoCoMo - CDMA</option>
												<option value="Telenor">Telenor</option>
												<option value="TTSL (GSM) (Tata Docomo)">TTSL (GSM) (Tata Docomo)</option>
												<option value="UNINOR (Telenor Unitech Ltd.)">UNINOR (Telenor Unitech Ltd.)</option>
												<option value="Videocon">Videocon</option>
												<option value="Virgin Mobile India">Virgin Mobile India</option>
												<option value="Vodafone">Vodafone</option>
											</select>
										</div>
										<div class="form-group">
											<label for="formrow-inputSeries">Number Series</label>
											<select id="formrow-inputSeries" name="series" class="form-control" required>
												<option value="">--Select--</option>
												<!--<option value="9006">9006</option>
												<option value="9162">9162</option>
												<option value="9199">9199</option>
												<option value="9546">9546</option>
												<option value="9572">9572</option>
												<option value="9631">9631</option>
												<option value="9661">9661</option>
												<option value="9771">9771</option>
												<option value="9801">9801</option>
												<option value="9931">9931</option>
												<option value="9934">9934</option>
												<option value="9939">9939</option>
												<option value="9955">9955</option>
												<option value="9973">9973</option>
												<option value="8002">8002</option>
												<option value="8084">8084</option>
												<option value="8292">8292</option>
												<option value="8294">8294</option>
												<option value="8521">8521</option>
												<option value="8757">8757</option>
												<option value="8809">8809</option>
												<option value="8969">8969</option>
												<option value="7033">7033</option>
												<option value="7070">7070</option>
												<option value="7091">7091</option>
												<option value="7250">7250</option>
												<option value="7319">7319</option>
												<option value="7463">7463</option>
												<option value="7543">7543</option>
												<option value="7739">7739</option>
												<option value="7762">7762</option>
												<option value="7763">7763</option>
												<option value="7632">7632</option>
												<option value="7258">7258</option>
												<option value="7462">7462</option>
												<option value="7541">7541</option>
												<option value="7700">7700</option>
												<option value="7781">7781</option>
												<option value="7295">7295</option>
												<option value="7321">7321</option>
												<option value="7368">7368</option>-->
											</select>
										</div>
										<div class="form-group">
											<label for="formrow-inputZip">Enter Quantity</label>
											<input type="number" class="form-control" name="quantity" id="formrow-inputQuantity" required>
										</div>
										<div class="button-items text-center">
											<button onclick="createCampaign()" type="submit" class="btn btn-success btn-effect w-lg waves-effect waves-light p-3">Generate</button>
											<button type="button" class="btn btn-secondary  btn-effect  w-lg waves-effect waves-light p-3">
												Reset
											</button>
										</div>
										<div class="text-center mt-4">
											<a href="javascript:void(0)" id="preferredClick" data-toggle="modal" data-target=".bs-example-modal-center">Click here</a> if you want to generate by entering your favourite number.
										</div>
									</div>
									<div class="col-md-2"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container-fluid -->
	</div>
	<!-- End Page-content -->
	
<script>
// State & Operator Ajax
function changeState(state){
var operator = $('#formrow-inputOperator').val();

if(state == null || operator == null){
	$('#formrow-inputSeries').html('<option value="">--Select--</option>');
	
	return false;
}

$('.load').show();
$.ajax({
	url: "<?php echo site_url('user/getNumberSeries') ?>",
	type: "POST",
	data: { state: state, operator: operator },
	success: function(res){
		$('.load').hide();
		if(res != "2"){
			$('#formrow-inputSeries').html(res);
		}else{
			$('#formrow-inputSeries').html('<option value="">--Select--</option>');
		}
	}
});
}
function changeOperator(operator){
var state = $('#formrow-inputState').val();

if(state == null || operator == null){
	$('#formrow-inputSeries').html('<option value="">--Select--</option>');
	
	return false;
}
$('.load').show();
$.ajax({
	url: "<?php echo site_url('user/getNumberSeries') ?>",
	type: "POST",
	data: { state: state, operator: operator },
	success: function(res){
		$('.load').hide();
		if(res != 2){
			$('#formrow-inputSeries').html(res);
		}else{
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Data Generator</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card neu">
                                <div class="card-body">
                                    <form id="form" method="POST" action="<?php echo site_url('user/createCampaign') ?>">
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="formrow-inputState">State</label>
                                                    <select id="formrow-inputState" name="state" class="form-control" required onchange="changeState(this.value)">
                                                        <option value="">--Choose State--</option>
                                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                        <option value="Assam">Assam</option>
                                                        <option value="Bihar">Bihar</option>
                                                        <option value="Chennai">Chennai</option>
                                                        <option value="Delhi">Delhi</option>
                                                        <option value="Gujarat">Gujarat</option>
                                                        <option value="Haryana">Haryana</option>
                                                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                        <option value="Karnataka">Karnataka</option>
                                                        <option value="Kerala">Kerala</option>
                                                        <option value="Kolkata">Kolkata</option>
                                                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                        <option value="Maharashtra &amp; Goa">Maharashtra &amp; Goa</option>
                                                        <option value="Mumbai">Mumbai</option>
                                                        <option value="North East">North East</option>
                                                        <option value="Orissa">Orissa</option>
                                                        <option value="Punjab">Punjab</option>
                                                        <option value="Rajasthan">Rajasthan</option>
                                                        <option value="Tamil Nadu">Tamil Nadu</option>
                                                        <option value="Uttar Pradesh (East)">Uttar Pradesh (East)</option>
                                                        <option value="Uttar Pradesh (West)">Uttar Pradesh (West)</option>
                                                        <option value="West Bengal">West Bengal</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="formrow-inputOperator">Operator</label>
                                                    <select id="formrow-inputOperator" name="operator" class="form-control" required onchange="changeOperator(this.value)">
                                                        <option value="">--Select--</option>
                                                        <option value="Aircel">Aircel</option>
                                                        <option value="Airtel">Airtel</option>
                                                        <option value="BSNL - CDMA">BSNL - CDMA</option>
                                                        <option value="BSNL - CellOne GSM">BSNL - CellOne GSM</option>
                                                        <option value="BSNL - GSM">BSNL - GSM</option>
                                                        <option value="DOLPHIN">DOLPHIN</option>
                                                        <option value="Etisalat DB Telecom India">Etisalat DB Telecom India</option>
                                                        <option value="Idea">Idea</option>
                                                        <option value="Loop Mobile">Loop Mobile</option>
                                                        <option value="MTS">MTS</option>
                                                        <option value="PING CDMA">PING CDMA</option>
                                                        <option value="Reliance - CDMA">Reliance - CDMA</option>
                                                        <option value="Reliance - GSM">Reliance - GSM</option>
                                                        <option value="Reliance Jio">Reliance Jio</option>
                                                        <option value="S Tel Ltd">S Tel Ltd</option>
                                                        <option value="Sistema Shyam TeleServices Ltd. (CDMA) (MTS)">Sistema Shyam TeleServices Ltd. (CDMA) (MTS)</option>
                                                        <option value="Spice Communications Ltd.">Spice Communications Ltd.</option>
                                                        <option value="Subrin Rintel">Subrin Rintel</option>
                                                        <option value="Tata DoCoMo - CDMA">Tata DoCoMo - CDMA</option>
                                                        <option value="Telenor">Telenor</option>
                                                        <option value="TTSL (GSM) (Tata Docomo)">TTSL (GSM) (Tata Docomo)</option>
                                                        <option value="UNINOR (Telenor Unitech Ltd.)">UNINOR (Telenor Unitech Ltd.)</option>
                                                        <option value="Videocon">Videocon</option>
                                                        <option value="Virgin Mobile India">Virgin Mobile India</option>
                                                        <option value="Vodafone">Vodafone</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="formrow-inputSeries">Number Series</label>
                                                    <select id="formrow-inputSeries" name="series" class="form-control" required>
                                                        <option value="">--Select--</option>
                                                        <!--<option value="9006">9006</option>
                                                        <option value="9162">9162</option>
                                                        <option value="9199">9199</option>
                                                        <option value="9546">9546</option>
                                                        <option value="9572">9572</option>
                                                        <option value="9631">9631</option>
                                                        <option value="9661">9661</option>
                                                        <option value="9771">9771</option>
                                                        <option value="9801">9801</option>
                                                        <option value="9931">9931</option>
                                                        <option value="9934">9934</option>
                                                        <option value="9939">9939</option>
                                                        <option value="9955">9955</option>
                                                        <option value="9973">9973</option>
                                                        <option value="8002">8002</option>
                                                        <option value="8084">8084</option>
                                                        <option value="8292">8292</option>
                                                        <option value="8294">8294</option>
                                                        <option value="8521">8521</option>
                                                        <option value="8757">8757</option>
                                                        <option value="8809">8809</option>
                                                        <option value="8969">8969</option>
                                                        <option value="7033">7033</option>
                                                        <option value="7070">7070</option>
                                                        <option value="7091">7091</option>
                                                        <option value="7250">7250</option>
                                                        <option value="7319">7319</option>
                                                        <option value="7463">7463</option>
                                                        <option value="7543">7543</option>
                                                        <option value="7739">7739</option>
                                                        <option value="7762">7762</option>
                                                        <option value="7763">7763</option>
                                                        <option value="7632">7632</option>
                                                        <option value="7258">7258</option>
                                                        <option value="7462">7462</option>
                                                        <option value="7541">7541</option>
                                                        <option value="7700">7700</option>
                                                        <option value="7781">7781</option>
                                                        <option value="7295">7295</option>
                                                        <option value="7321">7321</option>
                                                        <option value="7368">7368</option>-->
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="formrow-inputZip">Enter Quantity</label>
                                                    <input type="number" class="form-control" name="quantity" id="formrow-inputQuantity" required>
                                                </div>
                                                <div class="button-items text-center">
                                                    <button onclick="createCampaign()" type="submit" class="btn btn-success btn-effect w-lg waves-effect waves-light p-3">Generate</button>
                                                    <button type="button" class="btn btn-secondary  btn-effect  w-lg waves-effect waves-light p-3">
                                                        Reset
                                                    </button>
                                                </div>
                                                <div class="text-center mt-4">
                                                    <a href="javascript:void(0)" id="preferredClick" data-toggle="modal" data-target=".bs-example-modal-center">Click here</a> if you want to generate by entering your favourite number.
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
			
<script>
	// State & Operator Ajax
	function changeState(state){
		var operator = $('#formrow-inputOperator').val();
		
		if(state == null || operator == null){
			$('#formrow-inputSeries').html('<option value="">--Select--</option>');
		}
	}
});
}

$(document).ready(function(){
$('#form').submit(function(e){
	e.preventDefault();
});
});

function createCampaign(){
var state = $('#formrow-inputState').val();
var operator = $('#formrow-inputOperator').val();
var series = $('#formrow-inputSeries').val();
var quantity = $('#formrow-inputQuantity').val();

if(state != "" && operator != "" && series != "" && quantity != ""){
	swal("You have generated "+quantity+" numbers successfully. Do you want to start the campaign?", {
		buttons: {
		start_now: {
			text: "Start Now",
			value: "start_now",
		},
		schedule: {
			text: "Schedule",
			value: "schedule",
		},
		cancel: true,
		},
		icon: "success"
	})
	.then((value) => {
		switch (value) {
		
		case "start_now":
			start_campaign();
			break;
		
		case "schedule":
			schedule_campaign();
			break;
		
		default:
			swal.close();
		}
	});
}
}
//Campaigns Data
function start_campaign(){
var state = $('#formrow-inputState').val();
var operator = $('#formrow-inputOperator').val();
var series = $('#formrow-inputSeries').val();
var quantity = $('#formrow-inputQuantity').val();

if(state != "" && operator != "" && series != "" && quantity != ""){
	$('.load').show();
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('user/createCampaign') ?>",
		data: { state: state, operator:operator, series: series, quantity: quantity },
		success: function(res){
			$('.load').hide();
			if(res){
				swal("Your campaign has been created.", {icon: "success"});
				setTimeout(function(){ location.reload(); }, 2000);
			}else{
				swal("You do not have enough credits available.", {icon: "warning"});
			}
		}
	});
	
}
}

function schedule_campaign(){
swal({
		text: 'Confirm date and time?',
		content: {
		element: "input",
		attributes: {
			type: "datetime-local",
			// value: "<?php echo date('Y-m-d').''.date('H:iA') ?>",
			min: "<?php echo date('Y-m-d').'T'.date('h:i:s') ?>",
		},
		},
		button: {
		text: "Confirm",
		closeModal: false,
		},
	})
	.then(name => {
		if (!name) throw null;
		
		var state = $('#formrow-inputState').val();
		var operator = $('#formrow-inputOperator').val();
		var series = $('#formrow-inputSeries').val();
		var quantity = $('#formrow-inputQuantity').val();
		
		if(state != "" && operator != "" && series != "" && quantity != ""){
			$('.load').show();
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('user/scheduleCampaign') ?>",
				data: { state: state, operator:operator, series: series, quantity: quantity, date_time: name },
				success: function(res){
					$('.load').hide();
					if(res){
						swal("Your campaign has been scheduled.", {icon: "success"});
						setTimeout(function(){ location.reload(); }, 2000);
					}else{
						swal("You do not have enough credits available.", {icon: "warning"});
					}
				}
			});
			
		}
<<<<<<< HEAD
	})
	.catch(err => {
		if (err) {
		swal("Oh noes!", "The AJAX request failed!", "error");
		} else {
		swal.stopLoading();
		swal.close();
		}
	});
}
=======
		$('.load').show();
		$.ajax({
			url: "<?php echo site_url('user/getNumberSeries') ?>",
			type: "POST",
			data: { state: state, operator: operator },
			success: function(res){
				$('.load').hide();
				if(res != 2){
					$('#formrow-inputSeries').html(res);
				}else{
					$('#formrow-inputSeries').html('<option value="">--Select--</option>');
				}
			}
		});
	}
	
	$(document).ready(function(){
		$('#form').submit(function(e){
			e.preventDefault();
		});
	});
	
	function createCampaign(){
		var state = $('#formrow-inputState').val();
		var operator = $('#formrow-inputOperator').val();
		var series = $('#formrow-inputSeries').val();
		var quantity = $('#formrow-inputQuantity').val();
		
		if(state != "" && operator != "" && series != "" && quantity != ""){
			swal("You have generated "+quantity+" numbers successfully. Do you want to start the campaign?", {
			  buttons: {
				start_now: {
				  text: "Start Now",
				  value: "start_now",
				},
				schedule: {
				  text: "Schedule",
				  value: "schedule",
				},
				cancel: true,
			  },
			  icon: "success"
			})
			.then((value) => {
			  switch (value) {
			 
				case "start_now":
				  start_campaign();
				  break;
			 
				case "schedule":
				  schedule_campaign();
				  break;
			 
				default:
				  swal.close();
			  }
			});
		}
	}
	//Campaigns Data
	function start_campaign(){
		var state = $('#formrow-inputState').val();
		var operator = $('#formrow-inputOperator').val();
		var series = $('#formrow-inputSeries').val();
		var quantity = $('#formrow-inputQuantity').val();
		
		if(state != "" && operator != "" && series != "" && quantity != ""){
			$('.load').show();
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('user/createCampaign') ?>",
				data: { state: state, operator:operator, series: series, quantity: quantity },
				success: function(res){
					$('.load').hide();
					if(res){
						swal("Your campaign has been created.", {icon: "success"});
						setTimeout(function(){ location.reload(); }, 2000);
					}else{
						swal("You do not have enough credits available.", {icon: "warning"});
					}
				}
			});
			
		}
	}
	
	function schedule_campaign(){
		swal({
			  text: 'Confirm date and time?',
			  content: {
				element: "input",
				attributes: {
				  type: "datetime-local",
				  // value: "<?php echo date('Y-m-d').''.date('H:iA') ?>",
				  min: "<?php echo date('Y-m-d').'T'.date('h:i:s') ?>",
				},
			  },
			  button: {
				text: "Confirm",
				closeModal: false,
			  },
			})
			.then(name => {
			  if (!name) throw null;
			 
				var state = $('#formrow-inputState').val();
				var operator = $('#formrow-inputOperator').val();
				var series = $('#formrow-inputSeries').val();
				var quantity = $('#formrow-inputQuantity').val();
				
				if(state != "" && operator != "" && series != "" && quantity != ""){
					$('.load').show();
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('user/scheduleCampaign') ?>",
						data: { state: state, operator:operator, series: series, quantity: quantity, date_time: name },
						success: function(res){
							$('.load').hide();
							if(res){
								swal("Your campaign has been scheduled.", {icon: "success"});
								setTimeout(function(){ location.reload(); }, 2000);
							}else{
								swal("You do not have enough credits available.", {icon: "warning"});
							}
						}
					});
					
				}
			})
			.catch(err => {
			  if (err) {
				swal("Oh noes!", "The AJAX request failed!", "error");
			  } else {
				swal.stopLoading();
				swal.close();
			  }
			});
	}
>>>>>>> ee88083961fc024827943d0ffbd7bda59b23dd76
</script>