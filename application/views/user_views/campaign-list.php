<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Running Campaign</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="col-md-12">
                <div class="card neu">
                    <div class="card-body">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr class="tb-head">
                                    <th>No.</th>
                                    <th>ID</th>
                                    <th>Number of Data</th>
                                    <th> Time </th>
                                    <th> Date </th>
                                    <th> Status </th>
                                    <th> Full View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>C0001</td>
                                    <td>1000</td>
                                    <td>04:01 AM</td>
                                    <td>02-12-2020</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a data-toggle="modal" data-target=".bs-example-modal-xl" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2"><i class="bx bx-show font-size-16 align-middle mr-2"></i>View</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>C0001</td>
                                    <td>1000</td>
                                    <td>04:01 AM</td>
                                    <td>02-12-2020</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a data-toggle="modal" data-target=".bs-example-modal-xl" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2"><i class="bx bx-show font-size-16 align-middle mr-2"></i>View</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>C0001</td>
                                    <td>1000</td>
                                    <td>04:01 AM</td>
                                    <td>02-12-2020</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a data-toggle="modal" data-target=".bs-example-modal-xl" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2"><i class="bx bx-show font-size-16 align-middle mr-2"></i>View</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>C0001</td>
                                    <td>1000</td>
                                    <td>04:01 AM</td>
                                    <td>02-12-2020</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a data-toggle="modal" data-target=".bs-example-modal-xl" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2"><i class="bx bx-show font-size-16 align-middle mr-2"></i>View</a></td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>C0001</td>
                                    <td>1000</td>
                                    <td>04:01 AM</td>
                                    <td>02-12-2020</td>
                                    <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                    <td><a data-toggle="modal" data-target=".bs-example-modal-xl" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2"><i class="bx bx-show font-size-16 align-middle mr-2"></i>View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->