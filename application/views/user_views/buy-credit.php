<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="col-lg-12">
                <div class="card neu">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Buy Credit</h4>
                        <div id="progrss-wizard" class="twitter-bs-wizard">
                            <ul class="twitter-bs-wizard-nav nav-justified icon-effect nav nav-pills">
                                <li class="nav-item">
                                    <a href="#progress-seller-details" class="nav-link active" data-toggle="tab">
                                        <span class="step-number mr-2">01</span>
                                        Your Order
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#progress-company-document" class="nav-link" data-toggle="tab">
                                        <span class="step-number mr-2">02</span>
                                        Billing Details
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#progress-confirm-detail" class="nav-link" data-toggle="tab">
                                        <span class="step-number mr-2">03</span>
                                        Payment Method
                                    </a>
                                </li>
                                <!--   <li class="nav-item">
                                            <a href="#progress-confirm-detail" class="nav-link" data-toggle="tab">
                                                <span class="step-number mr-2">04</span>
                                                Confirm Detail
                                            </a>
                                        </li> -->
                            </ul>
                            <div class="tab-content twitter-bs-wizard-tab-content">
                                <div class="tab-pane active" id="progress-seller-details">
                                    <form>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Credit Card Type</label>
                                                    <select class="custom-select" name="card_type">
                                                        <option selected disabled>Select Card Type</option>
                                                        <option value="AE">Call Credit Coin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Credit Type</label>
                                                    <select class="custom-select" name="no_of_credit" id="no_of_credit">
                                                        <option selected disabled> Select Credit </option>
                                                        <?php //echo '<pre>'.print_r($credit_list).'</pre>'; 
                                                        foreach($credit_list as $cl){ ?>
                                                        <option value="<?php echo $cl->no_of_credit; ?>" rel="<?php echo $cl->price_per_credit; ?>"><?php echo $cl->no_of_credit; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" id="dvCredit" style="display:none;">
                                                <div class="form-group">
                                                    <label>Enter No Of Credit</label>
                                                    <input class="form-control" id="CreditCount" name="CreditCount" type="number" minlength="100000" maxlength="1999999999" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="pp_credit">Price Per Credit</label>
                                                    <input type="text" class="form-control" id="pp_credit" name="pp_credit" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="amount">Amount</label>
                                                    <input type="text" class="form-control" id="amount" name="amount" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="tax-18p">Tax (18.00%)</label>
                                                    <input type="text" class="form-control" id="tax-18p" name="tax-18p" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="net-amount">Net Amount</label>
                                                    <input type="text" class="form-control" id="net-amount" name="net-amount" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="progress-company-document">
                                    <div>
                                        <form>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="fullname">Full Name</label>
                                                        <input type="text" class="form-control" id="fullname" name="fullname">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="mobileno">Mobile No.</label>
                                                        <input type="text" class="form-control" id="mobileno" name="mobileno">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="Email" class="form-control" id="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="country">Country</label>
                                                        <input type="text" class="form-control" id="country" name="country">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="state">State</label>
                                                        <input type="text" class="form-control" id="state" name="state">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="city">City</label>
                                                        <input type="text" class="form-control" id="city" name="city">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="post-code">Post Code</label>
                                                        <input type="text" class="form-control" id="post-code" name="post-code">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <input type="text" class="form-control" id="address" name="address">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="progress-confirm-detail">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center">
                                                <div class="mb-4 mt-3">
                                                    <!-- <i class="mdi mdi-check-circle-outline text-success display-4"></i> -->
                                                    <h1 style="font-weight: 700; font-size: 48px" id="PayableAmount"></h1>
                                                </div>
                                                <div>
                                                    <h5>Total Payment Amount</h5>
                                                    <p class="text-muted mt-4">Payment Option</p>
                                                    <img src="<?php echo site_url(); ?>assets/user/images/others/credit-card.png">
                                                </div>
                                                <button class="pay-btn btn btn-lg mt-4 waves-effect"> Pay Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="pager wizard twitter-bs-wizard-pager-link">
                                <li class="previous"><a href="javascript: void(0);">Previous</a></li>
                                <li class="next"><a href="javascript: void(0);">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    <script language="javascript" type="text/javascript">
        $(document).ready(function(){
            $("select#no_of_credit").change(function(event){
                $('#pp_credit').val("");
                $('#amount').val("");
                $('#tax-18p').val("");
                $('#net-amount').val("");
                $('#PayableAmount').text("");

                var att_rel = $('#no_of_credit').find('option:selected').attr('rel');
                var att_val = $('#no_of_credit').find('option:selected').val();
                
                if(att_val == "Above"){ 
                    $("#dvCredit").show();
                } else {
                    $("#dvCredit").hide();
                    var amount = att_val * att_rel;
                    var tax_18p = amount * .18;
                    var total_amount = amount + tax_18p;
                    $('#pp_credit').val(att_rel);
                    $('#amount').val(amount);
                    $('#tax-18p').val(tax_18p);
                    $('#net-amount').val(total_amount);
                    $('#PayableAmount').text("₹"+total_amount);
                }
            });
        });

        $('#CreditCount').keyup(function() {
            var c_count = $(this).val();
            
            var att_rel = $('#no_of_credit').find('option:selected').attr('rel');
            delay(function(){
                // alert(c_count);
                if(c_count > 100000){
                    var amount = c_count * att_rel;
                    var tax_18p = amount * .18;
                    var total_amount = amount + tax_18p;
                    $('#pp_credit').val(att_rel);
                    $('#amount').val(amount);
                    $('#tax-18p').val(tax_18p);
                    $('#net-amount').val(total_amount);
                    $('#PayableAmount').text("₹"+total_amount);
                } else {
                    swal({
                        title: "Not Allowed!!!",
                        text: "No. of credit should be above '100000'",
                        icon: "warning",
                    });
                }
            }, 1000 );
        });

        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
    </script>