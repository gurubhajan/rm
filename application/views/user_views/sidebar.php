<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>
                <li>
                    <a href="dashboard" class="waves-effect m-2">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboards">Dashboards</span>
                    </a>
                </li>
                <li>
                    <a href="user-profile" class="waves-effect m-2">
                        <i class="bx bx-user-circle"></i>
                        <span key="t-dashboards">Profile Manager</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect m-2">
                        <i class="bx bx-dollar-circle"></i>
                        <span key="t-dashboards">Available Credit </span>
                        <span class="counter" id="lblCount"><?php echo (!empty($u_credit->available_credit))?$u_credit->available_credit:"0"; ?></span>
                    </a>
                </li>
                <li>
                    <a href="buy-credit" class="waves-effect m-2">
                        <i class="bx bx-dollar-circle"></i>
                        <span key="t-dashboards">Buy Credit</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect m-2">
                        <i class="bx bx-archive-out"></i>
                        <span key="t-ecommerce">Campaign</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="new-campaign" key="t-products">New Campaign</a></li>
                        <li><a href="running-campaign" key="t-product-detail">Running Campaign</a></li>
                        <li><a href="schedule-campaign" key="t-orders">Schedule Campaign</a></li>
                        <li><a href="campaign-list" key="t-orders">Campaign List</a></li>
                    </ul>
                </li>
                <li>
                    <a href="transaction-history" class="waves-effect m-2">
                        <i class="bx bx-history"></i>
                        <span key="t-crypto">Transaction History</span>
                    </a>
                </li>
                <li>
                    <a href="data-generator" class="waves-effect m-2">
                        <i class="bx bx-data"></i>
                        <span key="t-crypto">Data Generator</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect  m-2">
                        <i class="bx bx-notepad"></i>
                        <span key="t-ecommerce">Ticket Manager</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="create-ticket" key="t-products">Create Ticket</a></li>
                        <li><a href="ticket-list" key="t-product-detail">Ticket List</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
