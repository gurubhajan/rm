<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Profile Manager</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card neu">
                        <div class="card-body">
                            <?php 
                                  
                                echo $this->session->flashdata('profile_update_message'); 
                                $this->session->set_flashdata ('profile_update_message','');
                                
                            ?>
                            <form action="<?php echo site_url("user/update_profile") ?>" enctype="multipart/form-data" method="POST">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-lg-8">
                                       
                                        <div class="form-group">
                                            <label for="Name">First Name</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" required placeholder="Enter First Name" value="<?php echo $c_user[0]->firstname; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Last Name</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" required placeholder="Enter last name" value="<?php echo $c_user[0]->lastname; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="useremail">Email</label>
                                            <input type="email" class="form-control" id="useremail" name="useremail" required placeholder="Enter email" value="<?php echo $c_user[0]->email; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="mobile">Mobile Number</label>
                                            <input type="number" class="form-control" id="number" name="number" required placeholder="Enter mobile number" value="<?php echo $c_user[0]->mobile; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="userpassword">Password</label>
                                            <input type="text" class="form-control" id="userpassword" name="userpassword" required placeholder="Enter password" value="<?php echo $c_user[0]->password; ?>">
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="formrow-inputState">State</label>
                                            <select id="formrow-inputState" class="form-control">
                                                <option value="choose"> Choose State</option>
                                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                <option value="Assam">Assam</option>
                                                <option value="Bihar">Bihar</option>
                                                <option value="Chennai">Chennai</option>
                                                <option value="Delhi">Delhi</option>
                                                <option value="Gujarat">Gujarat</option>
                                                <option value="Haryana">Haryana</option>
                                                <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                <option value="Karnataka">Karnataka</option>
                                                <option value="Kerala">Kerala</option>
                                                <option value="Kolkata">Kolkata</option>
                                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                <option value="Maharashtra &amp; Goa">Maharashtra &amp; Goa</option>
                                                <option value="Mumbai">Mumbai</option>
                                                <option value="North East">North East</option>
                                                <option value="Orissa">Orissa</option>
                                                <option value="Punjab">Punjab</option>
                                                <option value="Rajasthan">Rajasthan</option>
                                                <option value="Tamil Nadu">Tamil Nadu</option>
                                                <option value="Uttar Pradesh (East)">Uttar Pradesh (East)</option>
                                                <option value="Uttar Pradesh (West)">Uttar Pradesh (West)</option>
                                                <option value="West Bengal">West Bengal</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="formrow-inputState">City</label>
                                            <select id="formrow-inputState" class="form-control">
                                                <option value="">Select City</option>
                                                <option value="Aircel">Aircel</option>
                                                <option value="Airtel">Airtel</option>
                                                <option value="BSNL - CDMA">BSNL - CDMA</option>
                                                <option value="BSNL - CellOne GSM">BSNL - CellOne GSM</option>
                                                <option value="BSNL - GSM">BSNL - GSM</option>
                                                <option value="DOLPHIN">DOLPHIN</option>
                                                <option value="Etisalat DB Telecom India">Etisalat DB Telecom India</option>
                                                <option value="Idea">Idea</option>
                                                <option value="Loop Mobile">Loop Mobile</option>
                                                <option value="MTS">MTS</option>
                                                <option value="PING CDMA">PING CDMA</option>
                                                <option value="Reliance - CDMA">Reliance - CDMA</option>
                                                <option value="Reliance - GSM">Reliance - GSM</option>
                                                <option value="Reliance Jio">Reliance Jio</option>
                                                <option value="S Tel Ltd">S Tel Ltd</option>
                                                <option value="Sistema Shyam TeleServices Ltd. (CDMA) (MTS)">Sistema Shyam TeleServices Ltd. (CDMA) (MTS)</option>
                                                <option value="Spice Communications Ltd.">Spice Communications Ltd.</option>
                                                <option value="Subrin Rintel">Subrin Rintel</option>
                                                <option value="Tata DoCoMo - CDMA">Tata DoCoMo - CDMA</option>
                                                <option value="Telenor">Telenor</option>
                                                <option value="TTSL (GSM) (Tata Docomo)">TTSL (GSM) (Tata Docomo)</option>
                                                <option value="UNINOR (Telenor Unitech Ltd.)">UNINOR (Telenor Unitech Ltd.)</option>
                                                <option value="Videocon">Videocon</option>
                                                <option value="Virgin Mobile India">Virgin Mobile India</option>
                                                <option value="Vodafone">Vodafone</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="formrow-inputZip">Address</label>
                                            <input type="text" class="form-control" id="formrow-inputZip" value="<?php echo $c_user[0]->address; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="formrow-inputZip">Pincode</label>
                                            <input type="number" class="form-control" id="formrow-inputZip" value="<?php echo $c_user[0]->pincode; ?>">
                                        </div> -->
                                        <div class="button-items text-center">
                                            <button type="submit" class="btn btn-success btn-effect w-lg waves-effect waves-light p-3">Save</button>
                                            <!-- <button type="button" class="btn btn-secondary  btn-effect  w-lg waves-effect waves-light p-3">
                                                Reset
                                            </button> -->
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
