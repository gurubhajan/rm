<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card bg-gradient-info btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">Total Data Upload Upto
                            </h4>
                            <h2 class="text-white">$ 15,0000</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card  bg-gradient-success  btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">Completed Data Verify Upto
                            </h4>
                            <h2 class="text-white">45,6334</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card bg-gradient-danger btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">To Incompleted Data Upto
                            </h4>
                            <h2 class="text-white">95,5741</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card neu">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Month Activity</h4>
                            <div id="column_chart" class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                    <!--end card-->
                </div>
                <div class="col-md-4">
                    <div class="card neu" style="height: 432px;">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Day Activity</h4>
                            <div id="donut_chart3" class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card neu">
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="tb-head">
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th> Subject </th>
                                        <th> Department </th>
                                        <th> Date </th>
                                        <th> Status </th>
                                        <th> Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach($tickets as $key => $val){ ?>
                                    <?php //echo '<pre>'.print_r($val).'</pre>'; ?>
                                    <tr class="tb-row">
                                        <td><?= $i ?></td>
                                        <td><?php echo $c_user[0]->firstname.' '.$c_user[0]->lastname; ?></td>
                                        <td><?= $val->user_email ?></td>
                                        <td><?= $val->subject ?></td>
                                        <td><?= $val->department ?></td>
                                        <td><?= $val->created_at ?></td>
                                        <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect"><?= $val->status ?></span> </td>
                                        <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                    </tr>
                                    <?php $i++; } ?>
                                    <!-- <tr class="tb-row">
                                        <td>1</td>
                                        <td>John Doe</td>
                                        <td>john@doe.com</td>
                                        <td>Check Demo</td>
                                        <td>Support</td>
                                        <td>02-12-2020 01:26:51</td>
                                        <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                        <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                    </tr>
                                    <tr class="tb-row">
                                        <td>1</td>
                                        <td>John Doe</td>
                                        <td>john@doe.com</td>
                                        <td>Check Demo</td>
                                        <td>Support</td>
                                        <td>02-12-2020 01:26:51</td>
                                        <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                        <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                    </tr>
                                    <tr class="tb-row">
                                        <td>1</td>
                                        <td>John Doe</td>
                                        <td>john@doe.com</td>
                                        <td>Check Demo</td>
                                        <td>Support</td>
                                        <td>02-12-2020 01:26:51</td>
                                        <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                        <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                    </tr>
                                    <tr class="tb-row">
                                        <td>1</td>
                                        <td>John Doe</td>
                                        <td>john@doe.com</td>
                                        <td>Check Demo</td>
                                        <td>Support</td>
                                        <td>02-12-2020 01:26:51</td>
                                        <td> <span class="badge badge-pill badge-soft-warning font-size-12 btn-effect">pending</span> </td>
                                        <td><a href="ticket-chat.html" type="button" class="btn btn-success btn-effect w-lg waves-effect waves-light p-2">View Discussion</a></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    