<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Transaction History</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="col-md-12">
                <div class="card neu">
                    <div class="card-body">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr class="tb-head">
                                    <th>#</th>
                                    <th> Order ID</th>
                                    <th> Date Time </th>
                                    <th> Transfer </th>
                                    <th>Creadits</th>
                                    <th>Price </th>
                                    <th>Tax (18%)</th>
                                    <th> Amount</th>
                                    <th> Type</th>
                                    <th> Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach($transactions as $key => $val){ ?>
                                <?php //echo '<pre>'.print_r($val).'</pre>'; ?>
                                <tr class="tb-row">
                                    <td><?= $i ?></td>
                                    <td><?= $val->order_id ?></td>
                                    <td><?= $val->created_at ?></td>
                                    <td><?= $val->transfer ?></td>
                                    <td><?= $val->credits ?></td>
                                    <td><?= $val->price ?></td>
                                    <td><?= $val->tax ?></td>
                                    <td><?= $val->amount ?></td>
                                    <td><?= $val->type ?></td>
                                    <td> <span class="badge badge-pill btn-effect badge-soft-<?php if($val->status == 'Pending'){echo 'warning';}elseif($val->status == 'Paid'){echo 'success';}elseif($val->status == 'Failed'){echo 'danger';} ?> font-size-12"><?= $val->status ?></span> </td>
                                </tr>
                                <?php $i++; } ?>
                                <!-- <tr class="tb-row">
                                    <td>1</td>
                                    <td>order_G7k7sEnhsGIbbn</td>
                                    <td>01-12-2020 03:29:46</td>
                                    <td>Purchase</td>
                                    <td>54000</td>
                                    <td>0.18000</td>
                                    <td>1620.00</td>
                                    <td>10620.00</td>
                                    <td> Call Credit Coin</td>
                                    <td> <span class="badge badge-pill btn-effect badge-soft-success font-size-12">Paid</span> </td>
                                </tr>
                                <tr class="tb-row">
                                    <td>1</td>
                                    <td>order_G7k7sEnhsGIbbn</td>
                                    <td>01-12-2020 03:29:46</td>
                                    <td>Purchase</td>
                                    <td>54000</td>
                                    <td>0.18000</td>
                                    <td>1620.00</td>
                                    <td>10620.00</td>
                                    <td> Call Credit Coin</td>
                                    <td> <span class="badge badge-pill btn-effect badge-soft-danger font-size-12">Failed</span></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->