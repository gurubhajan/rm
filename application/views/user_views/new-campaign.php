<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Start New Campaign</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card bg-gradient-info btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">Total Upload
                            </h4>
                            <h2 class="text-white">0</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card  bg-gradient-success  btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">Complete
                            </h4>
                            <h2 class="text-white">0</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center" style="color:white">
                    <div class="card bg-gradient-danger btn-effect card-img-holder text-white">
                        <div class="card-body">
                            <img src="<?php echo site_url(); ?>assets/user/images/circle.svg" class="card-img-absolute" alt="circle-image">
                            <h4 class="font-weight-normal text-white mb-3">Not Completed
                            </h4>
                            <h2 class="text-white">0</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card neu">
                        <div class="card-body">
                            <div>
                                <div class="fallback">
                                    <input type="file" id="Files" style="display:none;" class="form-control">
                                    <img src="<?php echo site_url(); ?>assets/user/images/uploadimg.PNG" id="imgFileUpload" style="width:100%;">
                                </div>
                            </div>
                            <div style="margin-top:10px;text-align:center;">
                                <label>If you want,you can also generate data.</label><a href="data-generator">Click Here</a>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
                <div class="col-lg-12">
                    <div class="card neu">
                        <div class="card-body">
                            <div class="">
                                <h6>Total Upload</h6>
                                <div class="progress btn-effect mb-4" style="height: 1.625rem;">
                                    <div class="progress-bar progress-bar-striped bg-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="70%" role="progressbar" style="width: 70%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <h6>Completed</h6>
                                <div class="progress btn-effect mb-4" style="height: 1.625rem;">
                                    <div class="progress-bar progress-bar-striped bg-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="25%" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <h6>Completed</h6>
                                <div class="progress btn-effect mb-4" style="height: 1.625rem;">
                                    <div class="progress-bar progress-bar-striped bg-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="5%" role="progressbar" style="width: 5%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    <script>
        $('#imgFileUpload').click(function () {
            $('#Files').click();
        });
    </script>
