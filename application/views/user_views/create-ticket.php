<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Data Generator</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card neu">
                        <div class="card-body">
                            <form class="form-horizontal" action="reg-ticket" id="createticket">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="formrow-department">Department</label>
                                            <select id="formrow-department" class="form-control" name="department" required>
                                                <option value="" selected disabled>--Select--</option>
                                                <option value="1">Support</option>
                                                <option value="2">Billing</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="formrow-subject">Subject</label>
                                            <input type="text" class="form-control" id="formrow-subject" name="subject">
                                        </div>
                                        <div class="form-group">
                                            <label>Message</label>
                                            <div>
                                                <textarea required="" name="message" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="button-items text-center">
                                            <button type="submit" class="btn btn-success btn-effect  w-lg waves-effect waves-light p-3">Create</button>
                                            <button type="button" class="btn btn-secondary btn-effect w-lg waves-effect waves-light p-3">
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    <script type="text/javascript">
        $('form#createticket').on('submit', function (e) {
            alert("Hi");
            e.preventDefault();

            $.ajax({
                type: 'post',
                dataType: "json",
                url: 'reg_ticket',
                data: $('form#createticket').serialize(),
                success: function (response) {
                        if (response.class == "success" ){
                            swal({
                                title: response.msg,
                                // text: "You clicked the button!",
                                icon: response.class,
                                button: "Ok",
                            });  
                            
                        } else {
                            swal({
                                title: response.msg,
                                // text: "You clicked the button!",
                                icon: response.class,
                                buttons: true,
                                dangerMode: true,
                            });
                        }
                    
                    // setTimeout(function(){
                    //     $("#msg").hide();
                    // },5000);
                    location.reload();
                }
            });
        });
    </script>
    <!-- <script>
        $( document ).ready(function() {
            // swal("Ticket created successfully.", {
            //     buttons: ["Ok"],
            // });
            swal({
                title: "Failed to create ticket.",
                // text: "You clicked the button!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            });
        });
    </script> -->