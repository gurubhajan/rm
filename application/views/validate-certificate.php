<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Validate Certificate</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		
		<!-- Vendor CSS Files -->
		<!-- <link href="assets/coming_soon/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
		<link href="assets/coming_soon/vendor/icofont/icofont.min.css" rel="stylesheet">

		<!-- Template Main CSS File -->
		<link href="assets/coming_soon/css/style.css" rel="stylesheet">
		<style>

		/* Remove the navbar's default margin-bottom and rounded borders */
		.navbar {
			margin-bottom: 0;
			border-radius: 0;
		}
		/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
		.row.content {height: 450px}
		/* Set gray background color and 100% height */
		.sidenav {
			padding-top: 20px;
			background-color: #f1f1f1;
			height: 100%;
		}
		.row.align-items-center.h-100 {
		    text-align: center;
		}
		.search_certificates{
			margin: 0 auto;
		}
		.search_certificates input{
			margin: 5px;
			border-radius:5px !important;
		}
		.search_certificates-btn{
			/*display: inline-flex;*/
			margin: 10px;
    		margin-left: 20px;
		}
		/* Set black background color, white text and some padding */
		/*footer {
			background-color: #555;
			color: white;
			padding: 15px;
		}*/
		/* On small screens, set height to 'auto' for sidenav and grid */
		@media screen and (max-width: 767px) {
			.sidenav {
				height: auto;
				padding: 15px;
			}
			.row.content {height:auto;}
		}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<!-- <a class="navbar-brand" href="#">Logo</a> -->
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="/">Home</a></li>
						<!-- <li><a href="#">About</a></li>
						<li><a href="#">Projects</a></li>
						<li><a href="#">Contact</a></li> -->
					</ul>
					<!-- <ul class="nav navbar-nav navbar-right">
						<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
					</ul> -->
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="container-fluid text-center">
				<div class="row content">
					
					<div class="col-sm-12 text-left">
						<h1>Welcome</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<hr>
						<section class="heading_area">
							<div class="row align-items-center h-100">
								<h1>Verify your report</h1>
								<h5>With our section ‘Verify your report’ you can access a digital copy of your report and verify its details. </h5>
								<form action="/view-certificate/" method="get" id="cert_form">
									<div class="search_certificates input-group mb-3 col-md-4">
										<input type="text" name="cid" placeholder="Enter your Certificate Unique ID" id="searchCertificateField" class="form-control">	
									</div>
									<div class="search_certificates-btn mb-3">
										<input type="submit" value="View A4 Size Certificate" class="btn btn-dark" id="a4view">
										<input type="submit" value="View Card Size Certificate" class="btn btn-dark" id="cardview">
									</div>
								</form>
							</div>
						</section>
					</div>
					
				</div>
			</div>
		</div>
		<footer id="footer">
		    <div class="container">
		      <div class="copyright">
		        &copy; Copyright <strong><span>IP GEM LABS INTERNATIONAL</span></strong>. All Rights Reserved
		      </div>
		      <div class="credits">
		        <!-- All the links in the footer should remain intact. -->
		        <!-- You can delete the links only if you purchased the pro version. -->
		        <!-- Licensing information: https://bootstrapmade.com/license/ -->
		        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-coming-soon-template-countdwon/ -->
		        Designed by <a href="https://fiverr.com/gurubhajan">Guru Bhajan Singh</a>
		      </div>
		    </div>
		</footer>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   		<script type="text/javascript">
			$(document).ready(function(){
				$("#cert_form input.btn").click(function (ev) {
					ev.preventDefault();
					var cart_id = $("#searchCertificateField").val();

					if ($(this).attr("value") == "View A4 Size Certificate") {            
					    if(cart_id != ""){
					        window.location.href = "/view-certificate/?cid="+cart_id;
					    } else {
					    	alert("Please enter the Unique Certificate ID before submit");
					    }
					}
					if ($(this).attr("value") == "View Card Size Certificate") {
						if(cart_id != ""){
					        window.location.href = "/view-certificate-card/?cid="+cart_id;
					    } else {
					    	alert("Please enter the Unique Certificate ID before submit");
					    }
					}
				});
			});
		</script>
			<!-- $('#a4view').click(function(e) {
				e.preventDefault();
				var cart_id = $("#searchCertificateField").val();
			    if(!cart_id){
			        window.location.href = "/view-certificate/?cid="+cart_id;
			    } else {
			    	alert("Please enter the Unique Certificate ID before submit");
			    }
			    return false;
			}); -->
		    <!-- // document.getElementById("a4view").onclick = function () {
		    // 	e.preventDefault();
		    // 	var cart_id = $("#searchCertificateField").val();
		    // 	if(!cart_id){
			   //      location.href = "/view-certificate/?cid="+cart_id;
			   //  } else {
			   //  	alert("Please enter the Unique Certificate ID before submit");
			   //  }
		    // };

		    // document.getElementById("cardview").onclick = function () {
		    // 	e.preventDefault();
		    // 	var cart_id = $("#searchCertificateField").val();
		    // 	if(!cart_id){
			   //      location.href = "/view-certificate-card/?cid="+cart_id;
			   //  } else {
			   //  	alert("Please enter the Unique Certificate ID before submit");
			   //  }
		    // }; -->
		</script>
	</body>
</html>