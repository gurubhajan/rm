<!DOCTYPE html>
<html>
<head>
	<style>
		*{
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		}
		img {
			vertical-align: middle;
		}
		body{
			color: #000;
			font-family: Arial, sans-serif;
			font-size: 14px;
			font-weight: 400;
			background-color: gainsboro
		}
		table tfoot{
			background-color: #101010;
			color: #fff;
		}
		.details-info,
		table td {			
		    border-collapse: collapse;
		}
		a{
			color: #5da7f7;
			text-decoration: underline;
		}
		.details-info th{
			background-color: #ffe49d;
			border: 1px solid #dddddd;
		}
		.details-info th h4{
			font-size: 14px;
			font-weight: 600;
			text-align: center;
		}
		.details-info td,
		.details-info th{
			font-size: 12px;
			color: #333;
			font-weight: 600;
			line-height: 22px;
			padding: 5px 10px;
		}
		.details-info td{
			border: 1px solid #e4e4e4;
			text-align: left;
		}
		.details-info tr:nth-child(odd){
 			background-color: #eee;
		}
	</style>
</head>
<body>

<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="max-width: 800px; margin: 0 auto;background-color: #fff;padding: 10px 30px;">
	<thead>
		<tr>
	      	<td>
	  	        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="col" align="left">
		            <tbody>
		                <tr>
		                   <td valign="middle" width="100%">
		                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                         <tbody>
		                            <tr>
		                               <td valign="middle">
		                               		<div style="display: flex;align-items: center;justify-content: center;">
		                               			<img src="<?php echo site_url(); ?>assets/images/logo.png" width="85" height="85" style="border-radius: 100px;margin-right:20px;">
		                               			<div>
			                               			<h1 style="margin-bottom:5px;font-size: 30px;font-weight: 400;">IP GEM LABS INTERNATIONAL</h1>
				                               		<span style="line-height: 18px;">The Premier Choice For Finished Diamond Jewellery & Precious Gem<br>Certification</span>
				                               	</div>
		                               		</div>
		                               </td>
		                            </tr>
		                         </tbody>
		                      </table>
		                   </td>
		                </tr>
		             </tbody>
	          	</table>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
	  	        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="col" align="left">
		            <tbody>
		                <tr>
		                   <td valign="middle" width="100%">
		                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                         <tbody>
		                            <tr>
		                               <td valign="top">
		                               		<div>
		                               			<h3 style="font-size: 20px;font-weight: 600;text-align: center;border: 1px solid #cccccc;margin-top: 15px;padding: 8px;border-bottom: 0;">ANALYSIS OF FINISHED JEWELLERY GOODS</h3>
		                               		</div>
		                               </td>
		                            </tr>
		                         </tbody>
		                      </table>
		                   </td>
		                </tr>
		             </tbody>
	          	</table>	
			</td>
		</tr>
		<tr>
			<td>
	  	        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left" class="details-info">
		            <tbody>
                 	 	<tr>
						    <th><!-- <?php echo '<pre>'.print_r($cert_details).'</pre>'; ?> -->
                           			<h4>CERTIFICATION DATE: <?php echo $cert_details->created_on; ?></h4>
                       		</th>
						    <th><h4>CERTIFICATION ID: <span><?php echo $cert_details->certificate_id; ?></span></h4>
						    </th> 
						</tr>
                        <tr>
                           <td>DESCRIPTION:</td>
                           <td><?php echo $cert_details->description; ?></td>
                        </tr>
                        <tr>
                           <td>DIAMOND WEIGHT:</td>
                           <td><?php echo $cert_details->diamond_weight; ?></td>
                        </tr>
                        <tr>
                           <td>GROSS WEIGHT:</td>
                           <td><?php echo $cert_details->gross_weight; ?></td>
                        </tr>
                        <tr>
                           <td>SHAPE:</td>
                           <td><?php echo $cert_details->shape; ?></td>
                        </tr>		                            
                        <tr>
                           <td>FINISH:</td>
                           <td><?php echo $cert_details->finish; ?></td>
                        </tr>		                            	
                        <tr>
                           <td>COLOUR:</td>
                           <td><?php echo $cert_details->colour; ?></td>
                        </tr>		                            			                            
                        <tr>
                           <td>CLARITY:</td>
                           <td><?php echo $cert_details->clarity; ?></td>
                        </tr>		                            			                            		                            
                        <tr>
                           <td>ADDITIONAL NOTES:</td>
                           <td><?php echo $cert_details->additional_notes; ?></td>
                        </tr>		                            			                            		                            		
		             </tbody>
	          	</table>
			</td>
		</tr>
		<tr>
			<td>
				<tbody>
					<tr>
						<td style="padding: 5px;"></td>
					</tr>
				</tbody>
			</td>
		</tr>
		<tr>
			<td style="border:2px solid #ccc;padding: 15px 15px;border-radius: 2px;">
	  	        <table cellpadding="0" cellspacing="0" border="0" width="70%" class="col" align="left">
		            <tbody>
		                <tr>
		                   <td valign="middle" width="100%">
		                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                         <tbody>
		                            <tr>
		                               <td valign="top">
		                               		<div>
		                               			<img src="<?php echo site_url(); ?>assets/images/small-bg.png">
		                               		</div>
		                               </td>
		                            </tr>
		                         </tbody>
		                      </table>
		                   </td>
		                </tr>
		             </tbody>
	          	</table>
	  	        <table cellpadding="0" cellspacing="0" border="0" width="30%" class="col" align="left">
		            <tbody>
		                <tr>
		                   <td valign="middle" width="100%">
		                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                         <tbody>
		                            <tr>
		                               <td valign="top">
		                      				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					                         	<tbody>
						                            <tr>
						                               <td valign="top">
						                        			<div style="display: flex;border: 2px solid #5da7f7;border-radius: 2px;height: 160px;align-items: center;"><img src="<?php echo site_url(); ?>uploads/product_img/<?php echo $cert_details->product_img; ?>" style="width: 100%;"></div>
						                        			<p style="font-size: 13px;border: 1px solid #333;text-align: center;margin: 5px 5px 0;padding: 6px;">IMAGE OF TRUE LIKENESS</p>
						                               </td>
						                            </tr>
					                         	</tbody>
					                      	</table>
					                      	<table cellpadding="0" cellspacing="0" border="0" width="100%">
					                         	<tbody>
						                            <tr>
						                               <td style="padding: 10px;"></td>
						                            </tr>
					                         	</tbody>
					                      	</table>
  		                      				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					                         	<tbody>
						                            <tr>
						                               <td valign="top">
						                        			<div style="display: block;border: 2px solid #5da7f7;border-radius: 2px;height: 40px;"><img src="<?php echo site_url(); ?>uploads/signature_img/<?php echo $cert_details->signature_img; ?>" style="width: 100%; height: 36px;"></div>
						                        			<p style="font-size: 12px;border: 1px solid #333;text-align: center;margin: 5px 14px 0;padding: 6px;">AUTHORIZED SIGNATURE</p>
						                               </td>
						                            </tr>
					                         	</tbody>
					                      </table>         	
		                               </td>
		                            </tr>
		                         </tbody>
		                      </table>
		                   </td>
		                </tr>
		             </tbody>
	          	</table>	          	
			</td>
		</tr>		
		<tr>
			<td>
				<div style="text-align: center;padding: 12px 0;font-size: 12px;line-height: 18px;">
					<p style="margin-bottom: 15px;">Please read (over leaf) these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the services our company provides. By accessing our website www.ipgemlab.com  and entering into a contract agreement, you agree to be bound by these Terms. If you disagree with any part of the terms listed here then do not access the service.</p>
				</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td>
				<div style="text-align: center;padding: 10px 0;font-size: 12px;line-height: 20px;">
					<p>HEAD OFFICE</p>
					<P>1 VICTORIA SQUARE, BIRMINGHAM, WEST MIDLANDS, UNITED KINGDOM <a href="#">WWW.IPGEMLAB.COM</a> <a href="mailto:info@ipgemlab.com">info@ipgemlab.com</a></P>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
</body>
</html>