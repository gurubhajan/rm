<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		}
		.bothside{
			width: 100%;
			display: flex;
			float: left;
			justify-content: space-between;
			align-items: center;
		}
		.ring-img {
		    display: block;
		    text-align: center;
		}
		body{
			font-family: arial, sans-serif;
		}
		.bank-card__side_front {
			background-color: #f0f0ee;
			padding: .2% .4%;
			box-shadow: 0 0 10px #f4f4f2;
			display: block;
			border: 1px solid #a29e97;
			border-radius: 10px;
			height: 205px;
			margin: 0 auto;
			width: 324px;
		}
		.card-details{
			display: flex;
			justify-content: center;
		}
		.logo{
			justify-content: center;
		    display: flex;
		    align-items: center;
		}
		.details-info td {
		    border: 1px solid #e4e4e4;
		    text-align: left;
		}
		.details-info td, 
		.details-info th {
		    padding: 2px 5px;
		    font-size: 7px;
		    text-transform: capitalize;
		    letter-spacing: -.2px;
		    color: #333;
		    font-weight: 600;
		    line-height: 6px;
		}
		.details-info tr:nth-child(odd) {
		    background-color: #eee;
		}
		.details-info th {
		    background-color: #ffe49d;
		    border: 1px solid #dddddd;
		}
		table{
			margin-bottom: 3px;
		    border-collapse: collapse;
		}
		.ring-img img{
			width: 50px;
			border: 1px solid #5da7f7;
			padding: 1px;
		}
		img {
			vertical-align: middle;
		}
		.ring-img p{
			font-size: 6px;
			border: 1px solid #333;
			text-align: center;
			padding: 1px;
		}
	</style>
</head>
<body>
	<div class="card-details">
		<div class="bank-card__side_front">
			<div class="logo">
				<img src="https://www.ipgemlabs.cashpiper.com/assets/images/logo.png" width="30x" height="30px" style="border-radius: 100px;margin-right: 6px;">
				<h3 style="font-size: 12px">IP GEM LABS INTERNATIONAL</h3>
			</div>
			<h2 style="font-size: 8px;font-weight: 600;text-align: center;border: 1px solid #cccccc;margin-top: 2px;padding: 1px 4px;border-bottom: 0;">ANALYSIS OF FINISHED JEWELLERY GOODS</h2>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left" class="details-info">
	            <tbody>
             	 	<tr>
					    <th><h4>certification date: <?php echo $cert_details->created_on; ?></h4></th>
					    <th><h4>certification id: <span><?php echo $cert_details->certificate_id; ?></span></h4></th> 
					</tr>
                    <tr>
                       <td>description:</td>
                       <td><?php echo $cert_details->description; ?> </td>
                    </tr>
                    <tr>
                       <td>diamond weight:</td>
                       <td><?php echo $cert_details->diamond_weight; ?></td>
                    </tr>
                    <tr>
                       <td>gross weight:</td>
                       <td><?php echo $cert_details->gross_weight; ?></td>
                    </tr>
                    <tr>
                       <td>shape:</td>
                       <td><?php echo $cert_details->shape; ?></td>
                    </tr>		                            
                    <tr>
                       <td>finish:</td>
                       <td><?php echo $cert_details->finish; ?></td>
                    </tr>		                            	
                    <tr>
                       <td>colour:</td>
                       <td><?php echo $cert_details->colour; ?></td>
                    </tr>		                            			                            
                    <tr>
                       <td>clarity:</td>
                       <td><?php echo $cert_details->clarity; ?></td>
                    </tr>		                            			                            		                            
                    <tr>
                       <td>additional notes:</td>
                       <td><?php echo $cert_details->additional_notes; ?></td>
                    </tr>		                            			                            		                            		
	             </tbody>
          	</table>
          	<div class="bothside">
          		<div class="ring-img">
          			<img src="<?php echo site_url(); ?>uploads/product_img/<?php echo $cert_details->product_img; ?>">
          			<p>IMAGE OF TRUE LIKENESS</p>
          		</div>
          		<div class="ring-img">
          			<img src="<?php echo site_url(); ?>uploads/signature_img/<?php echo $cert_details->signature_img; ?>">
          			<p>AUTHORIZED SIGNATURE</p>
          		</div>
          	</div>
          	<p style="font-size: 8px;text-align: center;">HEAD OFFICE 1 VICTORIA SQUARE, BIRMINGHAM, WEST MIDLANDS, UNITED KINGDOM WWW.IPGEMLAB.COM <a href="mailto:info@ipgemlab.com">info@ipgemlab.com</a></p>
		</div>
	</div>

</body>
</html>