<?php
class admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}

	public function selectRecord( $table, $data, $where )
	{
		$this->db->select($data);
		$this->db->from($table);
		if($where!=""){
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query;
	}

    public function selectRecordbyDesc( $table, $data, $where )
    {
        $this->db->select($data);
        $this->db->from($table);
        if($where!=""){
            $this->db->where($where);
        }
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query;
    }

	public function update($table,$data,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$data);
	}

    //Delete
    public function delete($id, $tabel) {
        $result = $this->db->delete($tabel, array('id' => $id));
        return $result;
    }

	public function check_user_status($email, $password) {
        $this->db->where('email', $email);
        // $this->db->where('password', MD5($password));
        
        $query = $this->db->get(TABLE_PREFIX.'_users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
			return $row;
        }
    }

    // login user
    public function login_user($email, $password) {
        $this->db->where('email', $email);
        $this->db->where('password', MD5($password));
        $this->db->where('role', 1);
        $query = $this->db->get(TABLE_PREFIX.'_users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
			
			// $this->db->select("id");
			// $this->db->from(TABLE_PREFIX."_stores");
			// $this->db->where('user_id',$row->id);
			// $q = $this->db->get();
			// $st_id = $q->row()->id;
			// $st_image = $q->row()->image;
			
            $userdata = array(
                'admin_id' => $row->id,
                'a_fname' => $row->firstname,
                'a_lname' => $row->lastname,
                'a_email' => $row->email,
                'a_mobile' => $row->mobile,
                'a_role' => $row->role,
                'a_status' => $row->status,
            );
            print_R($this->session->set_userdata($userdata));
            return true;
        }
        return false;
    }

    
    public function getUserDetails()
    {

        $response = array();
     
        // Select record
        $this->db->select('id,email,reg_at');
        $q = $this->db->get(TABLE_PREFIX.'_newsletter');
        $response = $q->result_array();
     
        return $response;
    }

    function uploadData()
    {
        $count=0;
        $fp = fopen($_FILES['get_file']['tmp_name'],'r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {
                $insert_csv = array();
                $insert_csv['Title'] = $csv_line[0];//remove if you want to have primary key,
                $insert_csv['Description'] = $csv_line[1];
                $insert_csv['Difficulty'] = $csv_line[2];
                $insert_csv['Rating'] = $csv_line[3];
                $insert_csv['Region'] = $csv_line[4];
                $insert_csv['Country'] = $csv_line[5];
                $insert_csv['Mountain'] = $csv_line[6];
                $insert_csv['SlideImage'] = $csv_line[7];

            }
            $i++;
            $data = array(
                'title' => $insert_csv['Title'] ,
                'description' => $insert_csv['Description'],
                'difficulty' => $insert_csv['Difficulty'],
                'rating' => $insert_csv['Rating'],
                'region' => $insert_csv['Region'],
                'country' => $insert_csv['Country'],
                'mountain' => $insert_csv['Mountain'],
                'slide_image' => $insert_csv['SlideImage']
               );
            $data['crane_features']=$this->db->insert(TABLE_PREFIX.'_treks', $data);
        }
        fclose($fp) or die("can't close file");
        $data['success']="success";
        return $data;
    }

    function uploadvendorData()
    {
        $count=0;
        $fp = fopen($_FILES['get_file']['tmp_name'],'r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {
                $insert_csv = array();
                $insert_csv['Name'] = $csv_line[0];//remove if you want to have primary key,
                $insert_csv['Email'] = $csv_line[1];
                $insert_csv['SiteLink'] = $csv_line[2];
                $insert_csv['ContactNo'] = $csv_line[3];
                $insert_csv['Rating'] = $csv_line[4];
                $insert_csv['Description'] = $csv_line[5];
                $insert_csv['Image'] = $csv_line[6];
            }
            $i++;
            $data = array(
                'v_name' => $insert_csv['Name'] ,
                'v_email' => $insert_csv['Email'],
                'site_link' => $insert_csv['SiteLink'],
                'contact_no' => $insert_csv['ContactNo'],
                'v_rating' => $insert_csv['Rating'], 
                'v_description' => $insert_csv['Description'], 
                'v_image' => $insert_csv['Image']
               );
            $data['crane_features']=$this->db->insert(TABLE_PREFIX.'_vendors', $data);
        }
        fclose($fp) or die("can't close file");
        $data['success']="success";
        return $data;
    }

}