<?php
class user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

	public function insert($table,$data){
		$this->db->insert($table,$data);
		
		return $this->db->insert_id();
	}

	public function selectRecord( $table, $data, $where )
	{
		$this->db->select($data);
		$this->db->from($table);
		if($where!=""){
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query;
	}

    public function selectRecordbyDesc( $table, $data, $where )
    {
        $this->db->select($data);
        $this->db->from($table);
        if($where!=""){
            $this->db->where($where);
        }
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query;
    }

	public function update($table,$data,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$data);
	}

    //Delete
    public function delete($id, $tabel) {
        $result = $this->db->delete($tabel, array('id' => $id));
        return $result;
    }

    public function check_user($email) {
        $this->db->where('email', $email);
        // $this->db->where('password', MD5($password));
        
        $query = $this->db->get(TABLE_PREFIX.'_users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row;
        }
    }

    // login user
    public function login_user($email, $password) {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('role', 0);
        $query = $this->db->get(TABLE_PREFIX.'_users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            
            $userdata = array(
                'user_id'   => $row->id,
                'u_fname'   => $row->firstname,
                'u_lname'   => $row->lastname,
                'u_email'   => $row->email,
                'u_mobile'  => $row->mobile,
                'u_role'    => $row->role,
                'u_status'  => $row->status,
                'u_state'   => $row->state,
                'u_city'    => $row->city,
                'u_address' => $row->address,
                'u_pincode' => $row->pincode,
            );
            print_R($this->session->set_userdata($userdata));
            return true;
        }
        return false;
    }
}